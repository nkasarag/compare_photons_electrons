import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)
import os

import pandas as pd
import shutil

import ROOT

import seaborn as sns


xlable_keys = {"r9": r"$5 \times 5$ $R_9$",
    "sieie": r"$5 \times 5$ $\sigma_{i\eta i\eta}$",
    "hadronicOverEm": r"H/E",
    "chargedHadronIso": r"Charged hadron isolation [GeV]",
    "neutralHadronIso": r"Neutral hadron isolation [GeV]",
    "photonIso": r"Photon isolation [GeV]",
    "trkIso": r"Track isolation [GeV]",
    "ecalIso": r"ECAL PF cluster isolation [GeV]",
    "hcalIso": r"HCAL PF cluster isolation [GeV]"
    }





def plot_2dhist(lst1, lst2, xlable, ylable, outpath, region, eve):

    c=ROOT.TCanvas()

    c.SetCanvasSize(500,400)

    c.SetLogz()

    ROOT.gStyle.SetOptStat(0)

    c.SetRightMargin(0.14);
    c.SetLeftMargin(0.14);
    c.SetTopMargin(0.06);


    rmax_keys = {"r9": 2,
    "sieie": min(max(lst1), 0.08),
    "chargedHadronIso": 80,
    "neutralHadronIso": 30,
    "photonIso": 100,
    "hadronicOverEm": 0.6,
    "trkIso": 80,
    "ecalIso": 40,
    "hcalIso": 40
    }

    if not os.path.exists(outpath): os.mkdir(outpath)
    if not os.path.exists(outpath+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", outpath+"index.php")
    if not os.path.exists(outpath+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", outpath+"res")

    sub_folder = outpath+"2D_dist/"
    if not os.path.exists(sub_folder): os.mkdir(sub_folder)
    if not os.path.exists(sub_folder+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", sub_folder+"index.php")
    if not os.path.exists(sub_folder+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", sub_folder+"res")

    path_to_save = "%s/%s/"%(sub_folder, eve)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)
    if not os.path.exists(path_to_save+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", path_to_save+"index.php")
    if not os.path.exists(path_to_save+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", path_to_save+"res")

    path_to_save2 = "%s/%s/"%(path_to_save, region)
    if not os.path.exists(path_to_save2): os.mkdir(path_to_save2)
    if not os.path.exists(path_to_save2+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", path_to_save2+"index.php")
    if not os.path.exists(path_to_save2+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", path_to_save2+"res")

    if region=="all": title = "%s;%s;%s"%(eve, xlable_keys[xlable], xlable_keys[ylable])

    else: title = "%s;%s;%s"%(eve, xlable_keys[xlable]+" (%s)"%region, xlable_keys[ylable]+" (%s)"%region)

    H_pion = ROOT.TH2F("H_pion",title,  100,min(lst1),rmax_keys[xlable], 100,min(lst2),rmax_keys[ylable])

    x = lst1
    y = lst2

    for i in range(x.size):
           H_pion.Fill(x[i], y[i])
        
    H_pion.GetXaxis().SetTitleSize(0.045)

    H_pion.GetYaxis().SetTitleSize(0.045)
    


    H_pion.Draw("COlZ")

    c.Draw()

    c.SaveAs("%s2d%s_%s_vs_%s_%s.png"%(path_to_save2, eve, ylable, xlable, region))
    c.SaveAs("%s2d%s_%s_vs_%s_%s.pdf"%(path_to_save2, eve, ylable, xlable, region))

    return path_to_save2



df = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/photon_GJets_30files.pickle")#.head(100000)
print("file1_read")

n = len(xlable_keys.keys())

cor_matrix_all = np.ones([n,n])
cor_matrix_EB = np.ones([n,n])
cor_matrix_EE = np.ones([n,n])



#for i in range(n):
#      cor_matrix_all[i,i] = 1
#      cor_matrix_EB[i,i] = 1
#      cor_matrix_EE[i,i] = 1


cor_matrix_EB_b = np.ones([n,n])
cor_matrix_all_b = np.ones([n,n])
cor_matrix_EE_b = np.ones([n,n])

#for i in range(n):
#      cor_matrix_all_b[i,i] = 1
#      cor_matrix_EB_b[i,i] = 1
#      cor_matrix_EE_b[i,i] = 1

var_list = list(xlable_keys.keys())

heat_var_list = ["r9",
    "sieie",
    "H/E",
    "ChaIso",
    "NeuIso",
    "PhoIso",
    "trkIso",
    "ECALIso",
    "HCALIso"
    ]

out_path = "/eos/user/n/nkasarag/www/folder/non_isolated_photonID/Photons_reweight_pt_eta_phi_30files/"


df_sub_signal = df.loc[(df.is_real==1)]
df_sub_background = df.loc[(df.is_real==0)]

for i in range(n):
      for j in range(i+1,n):
            path_s_all = plot_2dhist(df_sub_signal[var_list[i]].values, df_sub_signal[var_list[j]].values, var_list[i], var_list[j], out_path, "all", "GJets_real_photon")
            cor_matrix_all[j,i] = np.corrcoef(np.array(df_sub_signal[var_list[i]].values), np.array(df_sub_signal[var_list[j]].values))[1,0]
            cor_matrix_all[i, j] = cor_matrix_all[j,i]

            path_b_all = plot_2dhist(df_sub_background[var_list[i]].values, df_sub_background[var_list[j]].values, var_list[i], var_list[j], out_path, "all", "GJets_fake_photon")
            cor_matrix_all_b[j,i] = np.corrcoef(np.array(df_sub_background[var_list[i]].values), np.array(df_sub_background[var_list[j]].values))[1,0]
            cor_matrix_all_b[i,j] = cor_matrix_all_b[j,i]




df_sub_signal = df.loc[(df.is_real==1) & (df.subdetId==1)]
df_sub_background = df.loc[(df.is_real==0) & (df.subdetId==1)]

for i in range(n):
      for j in range(i+1,n):
            path_s_EB = plot_2dhist(df_sub_signal[var_list[i]].values, df_sub_signal[var_list[j]].values, var_list[i], var_list[j], out_path, "EB", "GJets_real_photon")
            cor_matrix_EB[j,i] = np.corrcoef(np.array(df_sub_signal[var_list[i]].values), np.array(df_sub_signal[var_list[j]].values))[1,0]
            cor_matrix_EB[i,j] = cor_matrix_EB[j,i]

            path_b_EB = plot_2dhist(df_sub_background[var_list[i]].values, df_sub_background[var_list[j]].values, var_list[i], var_list[j], out_path, "EB", "GJets_fake_photon")
            cor_matrix_EB_b[j,i] = np.corrcoef(np.array(df_sub_background[var_list[i]].values), np.array(df_sub_background[var_list[j]].values))[1,0]
            cor_matrix_EB_b[i,j] = cor_matrix_EB_b[j,i]


df_sub_signal = df.loc[(df.is_real==1) & (df.subdetId==2)]
df_sub_background = df.loc[(df.is_real==0) & (df.subdetId==2)]

for i in range(n):
      for j in range(i+1,n):
            path_s_EE = plot_2dhist(df_sub_signal[var_list[i]].values, df_sub_signal[var_list[j]].values, var_list[i], var_list[j], out_path, "EE", "GJets_real_photon")
            cor_matrix_EE[j,i] = np.corrcoef(np.array(df_sub_signal[var_list[i]].values), np.array(df_sub_signal[var_list[j]].values))[1,0]
            cor_matrix_EE[i,j] = cor_matrix_EE[j,i]

            path_b_EE = plot_2dhist(df_sub_background[var_list[i]].values, df_sub_background[var_list[j]].values, var_list[i], var_list[j], out_path, "EE", "GJets_fake_photon")
            cor_matrix_EE_b[j,i] = np.corrcoef(np.array(df_sub_background[var_list[i]].values), np.array(df_sub_background[var_list[j]].values))[1,0]
            cor_matrix_EE_b[i,j] = cor_matrix_EE_b[j,i]


#mask_ = (cor_matrix_all==-100)
def heat_map(a, lst, path):

    fig = plt.figure(figsize=[16,12])

    sns.heatmap(a, annot=True, fmt=".2f", xticklabels=lst, yticklabels=lst, cmap="viridis")#, mask= mask_)

    #plt.xlabel(fontsize=10)
    #plt.xlabel(fontsize=10)

    fig.show()
    fig.savefig("%s/%s.png"%(path, "corr"))
    fig.savefig("%s/%s.pdf"%(path, "corr"))
    fig.clf()



heat_map(cor_matrix_all, heat_var_list, path_s_all)
heat_map(cor_matrix_all_b, heat_var_list, path_b_all)
heat_map(cor_matrix_EB, heat_var_list, path_s_EB)
heat_map(cor_matrix_EB_b, heat_var_list, path_b_EB)
heat_map(cor_matrix_EE, heat_var_list, path_s_EE)
heat_map(cor_matrix_EE_b, heat_var_list, path_b_EE)
