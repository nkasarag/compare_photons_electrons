import ROOT
ROOT.gROOT.SetBatch(True)

# load FWLite C++ libraries
ROOT.gSystem.Load("libFWCoreFWLite.so");
ROOT.gSystem.Load("libDataFormatsFWLite.so");
ROOT.FWLiteEnabler.enable()
from DataFormats.FWLite import Handle, Events

#events = Events("root://xrootd-cms.infn.it///store/mc/Run3Winter22MiniAOD/GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8/MINIAODSIM/FlatPU0to70_122X_mcRun3_2021_realistic_v9-v2/2430000/0176f78e-294f-4ec1-9e8d-ddcbf471ae65.root")

#events =  Events("/eos/cms/store/group/phys_egamma/ec/fmausolf/GJet_Run3_MiniAOD/Pt-40toInf/GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8/GJetRun3_Pt-40toInf/221228_143544/0000/output_2.root")





###plot histogram

import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)

import os

def plot_hist(lst1, lst2, leg1, leg2, xlable, ylable, outpath, region):

    nbins=100

    rmax_keys = {"r9": 2,
    "sieie": min(max(max(lst1),max(lst2)), 0.08),
    "chargedHadronIso": 80,
    "neutralHadronIso": 30,
    "photonIso": 100,
    "relchargedHadronIso": 1.5,
    "relneutralHadronIso": 0.8,
    "relphotonIso": 2,
    "hadronicOverEm": 0.6,
    "pt": 300,
    "eta": 3,
    "phi": 3
    }

    rmax = rmax_keys[xlable]



    rmin = min(min(lst1),min(lst2))

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax))
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        label=leg2,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all": ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    else: ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)


    ax.set_ylabel(ylable, fontsize=18)
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s_%s.png"%(path_to_save, xlable, region))
    fig.savefig("%s%s_%s.pdf"%(path_to_save, xlable, region))
    fig.clf()


def plot_hist_log(lst1, lst2, leg1, leg2, xlable, ylable, outpath, region):

    nbins=100

    rmin = min(min(lst1),min(lst2))
    rmax = max(max(lst1),max(lst2))

    if xlable=="sieie": rmax = min(max(max(lst1),max(lst2)), 0.08)

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax))
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        label=leg2,
        ax=ax,
    )


    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s_log/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all": ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    else: ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)

    
    ax.set_ylabel(ylable, fontsize=18)
    ax.set_yscale('log')
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s_%s_log.png"%(path_to_save, xlable, region))
    fig.savefig("%s%s_%s_log.pdf"%(path_to_save, xlable, region))
    fig.clf()


def plot_2dhist(lst1, lst2, xlable, ylable, outpath, region, eve):

    nbins=100

    rmax_keys = {"r9": 2,
    "sieie": min(max(lst1), 0.08),
    "chargedHadronIso": 80,
    "neutralHadronIso": 30,
    "photonIso": 100,
    "relchargedHadronIso": 1.5,
    "relneutralHadronIso": 0.8,
    "relphotonIso": 2,
    "hadronicOverEm": 0.6,
    "pt": 300,
    "eta": 3,
    "phi": 3
    }


    print("\t corrcoef of %s and %s for %s_%s = "%(xlable, ylable, eve, region), np.corrcoef(np.array(lst1), np.array(lst2))[1,0], "\n")

    hist, xbin, ybin = np.histogram2d(lst1, lst2, bins=nbins, density=True, range=[[min(lst1), rmax_keys[xlable]], [min(lst2),rmax_keys[ylable]]])
    
    
    fig, ax = plt.subplots(figsize=(7, 7))
    hep.hist2dplot(
        hist,
        xbins=xbin,
        ybins=ybin,
        color="b",
        alpha=1,
        cmin=1e-4,
        edgecolor=None,
        cbarextend=True,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all":
        ax.set_xlabel(xlable_keys[xlable], fontsize=18)
        ax.set_ylabel(xlable_keys[ylable], fontsize=18)
    else:
        ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)
        ax.set_ylabel(xlable_keys[ylable]+" (%s)"%region, fontsize=18)

    ax.set_title(eve, fontsize=18)
    

    fig.show()
    fig.savefig("%s2d%s_%s_vs_%s_%s.png"%(path_to_save, eve, ylable, xlable, region))
    fig.savefig("%s2d%s_%s_vs_%s_%s.pdf"%(path_to_save, eve, ylable, xlable, region))
    fig.clf()




def plot_validation(lst1, leg1, xlable, ylable, outpath, valid_k):

    nbins=100

    #rmax_keys = {"genpt": 1,
    #"pt_by_genpt": r"$p_T$/Gen. $p_T$",
    #"dR": r"$\Delta$ R"
    #}

    rmax = max(lst1)



    rmin = min(lst1)

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s_%s/"%(outpath, "valid", leg1)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    ax.set_xlabel(valid_k[xlable], fontsize=18)
    ax.set_ylabel(ylable, fontsize=18)
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s.png"%(path_to_save, xlable))
    fig.savefig("%s%s.pdf"%(path_to_save, xlable))
    fig.clf()


def calculate_dR(photon_candidate):
     
    photon_reco = ROOT.TLorentzVector()
    photon_reco.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    photon_gen = ROOT.TLorentzVector()
    photon_gen.SetPtEtaPhiE(photon_candidate.genParticle().pt(), photon_candidate.genParticle().eta(), photon_candidate.genParticle().phi(), photon_candidate.genParticle().energy())
     
    return photon_reco.DeltaR(photon_gen)

def hadronic_fake_candidate(photon_candidate, genJets, genParticles):
    """
    This function checks on truth level if the photon candidate stems from a jet.
    Loops through all genJets and genParticles and checks if
    the closest object at generator level is a prompt photon, prompt electron, prompt muon or stems from a jet.
    Returns True / False
    """

    min_DeltaR = float(99999)

    photon_vector = ROOT.TLorentzVector()
    photon_vector.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    # print("\t\t Photon gen particle PDG ID:", photon_candidate.genParticle().pdgId())

    # this jet loop might be not needed... check later, but doesn't harm at this point
    jet_around_photon = False
    for genJet in genJets:
        # build four-vector to calculate DeltaR to photon
        genJet_vector = ROOT.TLorentzVector()
        genJet_vector.SetPtEtaPhiE(genJet.pt(), genJet.eta(), genJet.phi(), genJet.energy())

        DeltaR = photon_vector.DeltaR(genJet_vector)
        # print("\t\t INFO: gen jet eta, phi, delta R ", genJet.eta(), genJet.phi(), DeltaR)
        if DeltaR < 0.3:
            jet_around_photon = True

    is_prompt = False
    pdgId = 0
    pt_fake = float(99999)

    for genParticle in genParticles:

        if genParticle.pt() < 1: continue # threshold of 1GeV for interesting particles

        # build four-vector to calculate DeltaR to photon
        genParticle_vector = ROOT.TLorentzVector()
        genParticle_vector.SetPtEtaPhiE(genParticle.pt(), genParticle.eta(), genParticle.phi(), genParticle.energy())
        
        DeltaR = photon_vector.DeltaR(genParticle_vector)
        # print("\t\t INFO: gen particle eta, phi, delta R ", genParticle.eta(), genParticle.phi(), DeltaR)
        if DeltaR < min_DeltaR and DeltaR < 0.3:
            min_DeltaR = DeltaR
            pdgId = genParticle.pdgId()
            is_prompt = genParticle.isPromptFinalState()
            pt_fake = genParticle.pt()

    fakept_by_recopt = pt_fake/photon_candidate.pt()
            
    #print("\t pdgId=",pdgId)
    #print("\t is_prompt=",is_prompt)
        # print("\t\t INFO: PDG ID:", pdgId)

    prompt_electron = True if (abs(pdgId)==11 and is_prompt) else False
    prompt_photon = True if (pdgId==22 and is_prompt) else False
    prompt_muon = True if (abs(pdgId)==13 and is_prompt) else False
    
    #print("\t prompt_electron=",prompt_electron)
    #print("\t prompt_photon=",prompt_photon)
    #print("\t prompt_muon=",prompt_muon)
    
    if jet_around_photon and not (prompt_electron or prompt_photon or prompt_muon) and fakept_by_recopt<1.2 and fakept_by_recopt>0.8:
        return [True, fakept_by_recopt, min_DeltaR]
    else:
        return [False]






xlable_keys = {"r9": r"$5 \times 5$ $R_9$",
    "sieie": r"$5 \times 5$ $\sigma_{i\eta i\eta}$",
    "chargedHadronIso": r"Charged hadron isolation [GeV]",
    "neutralHadronIso": r"Neutral hadron isolation [GeV]",
    "photonIso": r"Photon isolation [GeV]",
    "relchargedHadronIso": r"Rel. charged hadron isolation",
    "relneutralHadronIso": r"Rel. neutral hadron isolation",
    "relphotonIso": r"Rel. photon isolation",
    "hadronicOverEm": r"H/E",
    "pt": r"$p_T$ [GeV]",
    "eta": r"$\eta$",
    "phi": r"$\phi$ [rad]"
    }


all_list_true = {}

EB_list_true = {}

EE_list_true = {}


all_list_fake = {}

EB_list_fake = {}

EE_list_fake = {}

for key in xlable_keys.keys():
    all_list_true[key] = []
    EB_list_true[key] = []
    EE_list_true[key] = []

    all_list_fake[key] = []
    EB_list_fake[key] = []
    EE_list_fake[key] = []





valid_keys = {"genpt": r"Gen. $p_T$ [GeV]",
    "pt_by_genpt": r"$p_T$/Gen. $p_T$",
    "dR": r"$\Delta$ R"
}

valid = {}
for key in valid_keys.keys():
    valid[key] = []


valid_keys_b = {
    "fakept_by_recopt": r"Fake photon $p_T$/ Reco. $p_T$",
    "dR": r"$\Delta$ R"
}

valid_b = {}
for key in valid_keys_b.keys():
    valid_b[key] = []



photonHandle, photonLabel = Handle("std::vector<pat::Photon>"), "slimmedPhotons"
RecHitHandle, RecHitLabel = Handle("edm::SortedCollection<EcalRecHit,edm::StrictWeakOrdering<EcalRecHit> >"), "reducedEgamma:reducedEBRecHits" 
genParticlesHandle, genParticlesLabel = Handle("std::vector<reco::GenParticle>"), "prunedGenParticles"
genJetsHandle, genJetsLabel = Handle("std::vector<reco::GenJet>"), "slimmedGenJets"


path_GJet_files = "/eos/cms/store/group/phys_egamma/ec/nkasarag/GluGluHToGG/GluGluHToGG_M125_TuneCP5_13TeV-amcatnloFXFX-pythia8/GluGluHToGG_withPU_withRho/231106_135157/0000/"

filenames = os.listdir(path_GJet_files)
    # just take one file for tests
filenames = filenames[:1]

file_num = 0

for filename in filenames:


    file_num+=1
    print("\n \t ### processing file number: %s ###"%str(file_num))

    events = Events(path_GJet_files+filename)



    stop_index = 1
    for i,event in enumerate(events):

            if i == stop_index: 
                break 
        
            if i % 1000 == 0:
                print("\t \t INFO: processing event", i)

            event.getByLabel(photonLabel, photonHandle)
            event.getByLabel(RecHitLabel, RecHitHandle)
            event.getByLabel(genParticlesLabel, genParticlesHandle)
            event.getByLabel(genJetsLabel, genJetsHandle)

            genJets = genJetsHandle.product()
            genParticles = genParticlesHandle.product()

            #print(photonHandle.product())
        
            #print(i, " genJets: ", genJets)
            #print(i, " genParticles: ", genParticles)


            #print("\n \t event =",i+1)
            #
            for photon in photonHandle.product():
                #print(dir(photon))
                #print(photon.photonID("cutBasedPhotonID-Fall17-94X-V1-loose"))
                print(dir(photon.photonID("")))
                #print(dir(photon.full5x5_showerShapeVariables()))
                #print(dir(photon.photonID()))
            #    #print("\t et = %s"%photon.et())
            #    #print("\t pt = %s"%photon.pt())
            #    #print("\t energy = %s"%photon.energy())
            #    #print("\t r9 = %s"%photon.full5x5_r9())
            #    #print("\t sieie = %s"%photon.full5x5_sigmaIetaIeta())
            #    #print("\t hadronicOverEm = %s"%photon.hadronicOverEm())
            #    #print("\t chargedHadronIso = %s"%photon.chargedHadronIso())
            #    #print("\t neutralHadronIso = %s"%photon.neutralHadronIso())
            #    #print("\t photonIso = %s"%photon.photonIso())
            #    #print("\t subdetId = %s"%photon.superCluster().seed().seed().subdetId(), "\t photonEta = %s"%photon.eta())
#
            #    is_real = False
            #    is_hadronic_fake = False
            #    if photon.pt() < 20: continue
            #    
            #    try:
            #        pdgId = photon.genParticle().pdgId()
            #        if pdgId == 22:
            #            is_real = True
            #
            #    except ReferenceError:
            #        is_hadronic_fake = hadronic_fake_candidate(photon, genJets, genParticles)[0]
   #