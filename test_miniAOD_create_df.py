import ROOT
ROOT.gROOT.SetBatch(True)

# load FWLite C++ libraries
ROOT.gSystem.Load("libFWCoreFWLite.so");
ROOT.gSystem.Load("libDataFormatsFWLite.so");
ROOT.FWLiteEnabler.enable()
from DataFormats.FWLite import Handle, Events

import pandas as pd







###plot histogram

import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)

import os



def calculate_dR(photon_candidate):
     
    photon_reco = ROOT.TLorentzVector()
    photon_reco.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    photon_gen = ROOT.TLorentzVector()
    photon_gen.SetPtEtaPhiE(photon_candidate.genParticle().pt(), photon_candidate.genParticle().eta(), photon_candidate.genParticle().phi(), photon_candidate.genParticle().energy())
     
    return photon_reco.DeltaR(photon_gen)


def hadronic_fake_candidate(photon_candidate, genJets, genParticles):
    """
    This function checks on truth level if the photon candidate stems from a jet.
    Loops through all genJets and genParticles and checks if
    the closest object at generator level is a prompt photon, prompt electron, prompt muon or stems from a jet.
    Returns True / False
    """

    min_DeltaR = float(99999)

    photon_vector = ROOT.TLorentzVector()
    photon_vector.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    # print("\t\t Photon gen particle PDG ID:", photon_candidate.genParticle().pdgId())

    # this jet loop might be not needed... check later, but doesn't harm at this point
    jet_around_photon = False
    for genJet in genJets:
        # build four-vector to calculate DeltaR to photon
        genJet_vector = ROOT.TLorentzVector()
        genJet_vector.SetPtEtaPhiE(genJet.pt(), genJet.eta(), genJet.phi(), genJet.energy())

        DeltaR = photon_vector.DeltaR(genJet_vector)
        # print("\t\t INFO: gen jet eta, phi, delta R ", genJet.eta(), genJet.phi(), DeltaR)
        if DeltaR < 0.3:
            jet_around_photon = True

    is_prompt = False
    pdgId = 0
    pt_fake = float(99999)

    for genParticle in genParticles:

        if genParticle.pt() < 1: continue # threshold of 1GeV for interesting particles

        # build four-vector to calculate DeltaR to photon
        genParticle_vector = ROOT.TLorentzVector()
        genParticle_vector.SetPtEtaPhiE(genParticle.pt(), genParticle.eta(), genParticle.phi(), genParticle.energy())
        
        DeltaR = photon_vector.DeltaR(genParticle_vector)
        # print("\t\t INFO: gen particle eta, phi, delta R ", genParticle.eta(), genParticle.phi(), DeltaR)
        if DeltaR < min_DeltaR and DeltaR < 0.1:
            min_DeltaR = DeltaR
            pdgId = genParticle.pdgId()
            is_prompt = genParticle.isPromptFinalState()
            pt_fake = genParticle.pt()

    fakept_by_recopt = pt_fake/photon_candidate.pt()
            
    #print("\t pdgId=",pdgId)
    #print("\t is_prompt=",is_prompt)
        # print("\t\t INFO: PDG ID:", pdgId)

    prompt_electron = True if (abs(pdgId)==11 and is_prompt) else False
    prompt_photon = True if (pdgId==22 and is_prompt) else False
    prompt_muon = True if (abs(pdgId)==13 and is_prompt) else False
    
    #print("\t prompt_electron=",prompt_electron)
    #print("\t prompt_photon=",prompt_photon)
    #print("\t prompt_muon=",prompt_muon)
    
    if jet_around_photon and not (prompt_electron or prompt_photon or prompt_muon) and fakept_by_recopt<1.2 and fakept_by_recopt>0.8:
        return [1, pt_fake, min_DeltaR]
    else:
        return [0, None, None]
    












xlable_keys = {
    "energyRaw": [],
    "r9": [],
    "sieie": [],
    "etaWidth": [],
    "phiWidth": [],
    "sieip": [],
    "s4": [],
    "hoe": [],
    "ecalPFClusterIso": [],
    "trkSumPtHollowConeDR03": [],
    "trkSumPtSolidConeDR04": [],
    "pfChargedIso": [],
    "pfChargedIsoWorstVtx": [],
    "esEffSigmaRR": [],
    "esEnergyOverRawE": [],
    "hcalPFClusterIso": [],
    "pt": [],
    "eta": [],
    "phi": [],
    "rho": [],
    "subdetId": [],
    "is_photon": [],
    "PhotonMVAEstimatorRunIIFall17v2Values": [],
    "isConverted": []
    }









photonHandle, photonLabel = Handle("std::vector<pat::Photon>"), "slimmedPhotons"
RecHitHandle, RecHitLabel = Handle("edm::SortedCollection<EcalRecHit,edm::StrictWeakOrdering<EcalRecHit> >"), "reducedEgamma:reducedEBRecHits" 
genParticlesHandle, genParticlesLabel = Handle("std::vector<reco::GenParticle>"), "prunedGenParticles"
genJetsHandle, genJetsLabel = Handle("std::vector<reco::GenJet>"), "slimmedGenJets"
#pileupHandle, pileupLabel = Handle("std::vector<PileupSummaryInfo>"), "slimmedAddPileupInfo"
rhoHandle, rhoLabel = Handle("double"), "fixedGridRhoAll"


#path_GJet_files = "/eos/user/n/nkasarag/PhD/compare_JGet_DY_photon_objects/GJet_Run3_MiniAOD/Pt-40toInf/GJet_PT-40_DoubleEMEnriched_MGG-80_TuneCP5_13p6TeV_pythia8/GJetRun3_Pt-40toInf/230828_104237/0000/"
path_GJet_files = "/eos/cms/store/group/phys_egamma/ec/nkasarag/GluGluHToGG/GluGluHToGG_M125_TuneCP5_13TeV-amcatnloFXFX-pythia8/GluGluHToGG_withPU_withRho/231106_135157/0000/"

filenames = os.listdir(path_GJet_files)
    # just take one file for tests
#filenames = filenames[:1]

file_num = 0

for filename in filenames:


    file_num+=1
    print("\n \t ### processing file number: %s ###"%str(file_num))

    events = Events(path_GJet_files+filename)



    stop_index = -10
    for i,event in enumerate(events):

            if i == stop_index:
                break
        
            if i % 1000 == 0:
                print("\t \t INFO: processing event", i)

            event.getByLabel(photonLabel, photonHandle)
            event.getByLabel(RecHitLabel, RecHitHandle)
            event.getByLabel(genParticlesLabel, genParticlesHandle)
            event.getByLabel(genJetsLabel, genJetsHandle)
            event.getByLabel(rhoLabel, rhoHandle)

            genJets = genJetsHandle.product()
            genParticles = genParticlesHandle.product()

        
        
            
            for photon in photonHandle.product():

                #print(photon.superCluster().rawEnergy()) #
                #print(photon.superCluster().etaWidth()) #
                #print(photon.superCluster().phiWidth()) #
                #print(photon.trkSumPtHollowConeDR03()) #
                #print(photon.trkSumPtSolidConeDR04())

                #print("sieip: ", photon.full5x5_showerShapeVariables().sigmaIetaIphi)
                #print("s4: ", photon.full5x5_showerShapeVariables().e2x2/photon.full5x5_showerShapeVariables().e5x5)
                #print("pfChargedIsoWorstVtx: ", photon.chargedHadronWorstVtxIso())
                #print("esEffSigmaRR: ", photon.full5x5_showerShapeVariables().effSigmaRR )
                #print("esEnergyOverRawE: ", photon.superCluster().preshowerEnergy()/photon.superCluster().rawEnergy())

                


                #print(photon.eta())
                #print(photon.superCluster().eta(), "\n")    




                is_real = 0
                is_hadronic_fake = 0
                is_converted = 0
                is_jet_around = 0
                gen_pt_ = float(99999)
                dR_ = float(99999)
                if photon.pt() < 20: continue
                

                try:
                    pdgId = photon.genParticle().pdgId()
                    if pdgId == 22:
                        is_real = 1
                        is_converted = ((not photon.conversions().empty()) or (not photon.conversionsOneLeg().empty()))
            
                except ReferenceError:
                    pass
                    #is_hadronic_fake = hadronic_fake_candidate(photon, genJets, genParticles)[0]
                    

                #if is_real: 
                #    gen_pt_ = photon.genParticle().pt()
                #    dR_ = calculate_dR(photon)

                #if is_hadronic_fake:
                #    fake = hadronic_fake_candidate(photon, genJets, genParticles)
                #    gen_pt_ = fake[1]
                #    dR_ = fake[2]





                if ((is_real==1)):# and (not is_converted)):

                    #print(photon.photonID("mvaPhoID-RunIIFall17-v2-wp80"))
                    #print(photon.userFloat("PhotonMVAEstimatorRunIIFall17v2Values"))


                    xlable_keys["energyRaw"].append(photon.superCluster().rawEnergy())
                    xlable_keys["r9"].append(photon.full5x5_r9())
                    xlable_keys["sieie"].append(photon.full5x5_sigmaIetaIeta())
                    xlable_keys["etaWidth"].append(photon.superCluster().etaWidth())
                    xlable_keys["phiWidth"].append(photon.superCluster().phiWidth())
                    xlable_keys["sieip"].append(photon.full5x5_showerShapeVariables().sigmaIetaIphi)
                    xlable_keys["s4"].append(photon.full5x5_showerShapeVariables().e2x2/photon.full5x5_showerShapeVariables().e5x5)
                    xlable_keys["hoe"].append(photon.hadronicOverEm())
                    xlable_keys["ecalPFClusterIso"].append(photon.ecalPFClusterIso())
                    xlable_keys["trkSumPtHollowConeDR03"].append(photon.trkSumPtHollowConeDR03())
                    xlable_keys["trkSumPtSolidConeDR04"].append(photon.trkSumPtSolidConeDR04())
                    xlable_keys["pfChargedIso"].append(photon.chargedHadronIso())
                    xlable_keys["pfChargedIsoWorstVtx"].append(photon.chargedHadronWorstVtxIso())
                    xlable_keys["esEffSigmaRR"].append(photon.full5x5_showerShapeVariables().effSigmaRR)
                    xlable_keys["esEnergyOverRawE"].append(photon.superCluster().preshowerEnergy()/photon.superCluster().rawEnergy())
                    xlable_keys["hcalPFClusterIso"].append(photon.hcalPFClusterIso())
                    
                    xlable_keys["pt"].append(photon.pt())
                    xlable_keys["eta"].append(photon.eta())
                    xlable_keys["phi"].append(photon.phi())
                    xlable_keys["rho"].append(rhoHandle.product()[0])
                    xlable_keys["subdetId"].append(photon.superCluster().seed().seed().subdetId())
                    xlable_keys["isConverted"].append(is_converted)

                    


                    

                    xlable_keys["is_photon"].append(is_real)
                    xlable_keys["PhotonMVAEstimatorRunIIFall17v2Values"].append(photon.userFloat("PhotonMVAEstimatorRunIIFall17v2Values"))





path = "/eos/user/n/nkasarag/non_isolated_photon_id/"
filename = "photon_for_NFlow_withRho_MVA_conv_info"
#filename = "test_unconverted_photon"

df = pd.DataFrame(xlable_keys)



df.to_pickle("%s%s.pickle"%(path, filename))#, protocol=5)
print(df.tail())
print(df.r9.values.size)


#
