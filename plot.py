import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)
import os

import pandas as pd
import shutil


def plot_hist(lst1, lst2, lst3, w1, w2, w3, leg1, leg2, leg3, xlable, ylable, outpath, eta_range, pt_range):

    #nbins=50

    range_keys = {"r9":  [0.1, 1.2, 25],
    "sieie": [0, 0.03 if eta_range[1]<1.5 else 0.08, 70],
    "chargedHadronIso": [0, 50, 40],
    "neutralHadronIso": [0, 25, 40],
    "photonIso": [0, 60, 40],
    "relchargedHadronIso": [0, 1.0, 40],
    "relneutralHadronIso": [0, 0.6, 40],
    "relphotonIso": [0, 1.5, 40],
    "hadronicOverEm": [0, 0.5, 40],
    "pt": [40, 200, 25],#[pt_range[0], pt_range[1], 50],
    "eta": [-2.5, 2.5, 20],
    "phi": [-3.2, 3.2, 8],
    "trkIso": [0, 10, 40],
    "ecalIso": [0, 15, 40],
    "hcalIso": [0, 15, 40]
    }

    rmin = range_keys[xlable][0]
    rmax = range_keys[xlable][1]
    nbins = range_keys[xlable][2]



    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax), weights=w1)
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax), weights=w2)
    hist3, bins3 = np.histogram(lst3, bins=nbins, density=True, range=(rmin, rmax), weights=w3)
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="blue",
        label=leg2,
        ax=ax,
    )

    hep.histplot(
        hist3,
        bins=bins3,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        linestyle="--",
        label=leg3,
        ax=ax,
    )

    region = "eta"+str(eta_range) + "__pt"+str(pt_range)
    region = region.replace(" ","")
    region = region.replace(".","p")
    region= region.replace("[","_")
    region = region.replace("]","")
    region = region.replace(",","to")
    #print(region)

    if not os.path.exists(outpath): os.mkdir(outpath)
    if not os.path.exists(outpath+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", outpath+"index.php")
    if not os.path.exists(outpath+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", outpath+"res")

    sub_folder = outpath+"1D_dist/"
    if not os.path.exists(sub_folder): os.mkdir(sub_folder)
    if not os.path.exists(sub_folder+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", sub_folder+"index.php")
    if not os.path.exists(sub_folder+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", sub_folder+"res")

    path_to_save = "%s/%s/"%(sub_folder, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)
    if not os.path.exists(path_to_save+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", path_to_save+"index.php")
    if not os.path.exists(path_to_save+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", path_to_save+"res")

 

    ax.set_xlim((rmin, rmax))

    ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    ax.set_ylabel(ylable, fontsize=18)
    #ax.set_xlim(rmin, rmax)

    t = r"%s$\leq$|$\eta$|<%s    %s GeV$\leq$$p_T$ <%s GeV"%(eta_range[0],eta_range[1],pt_range[0],pt_range[1])

    if pt_range[1]==999999: t = r"%s$\leq$|$\eta$|<%s    $p_T\geq$%s GeV"%(eta_range[0],eta_range[1],pt_range[0])
    
    ax.set_title(t)
    
    ax.legend(fontsize=15)
    fig.savefig("%s%s.png"%(path_to_save, xlable))
    fig.savefig("%s%s.pdf"%(path_to_save, xlable))
    fig.clf()



def plot_hist_log(lst1, lst2, lst3, w1, w2, w3, leg1, leg2, leg3, xlable, ylable, outpath, eta_range, pt_range):

    #nbins=50

    range_keys = {"r9":  [0.1, 1.2, 25],
    "sieie": [0, 0.03 if eta_range[1]<1.5 else 0.08, 70],
    "chargedHadronIso": [0, 50, 40],
    "neutralHadronIso": [0, 25, 40],
    "photonIso": [0, 60, 40],
    "relchargedHadronIso": [0, 1.0, 40],
    "relneutralHadronIso": [0, 0.6, 40],
    "relphotonIso": [0, 1.5, 40],
    "hadronicOverEm": [0, 0.5, 40],
    "pt": [40, 200, 25],#[pt_range[0], pt_range[1], 50],
    "eta": [-2.5, 2.5, 20],
    "phi": [-3.2, 3.2, 8],
    "trkIso": [0, 10, 40],
    "ecalIso": [0, 15, 40],
    "hcalIso": [0, 15, 40]
    }

    rmin = range_keys[xlable][0]
    rmax = range_keys[xlable][1]
    nbins = range_keys[xlable][2]


    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax), weights=w1)
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax), weights=w2)
    hist3, bins3 = np.histogram(lst3, bins=nbins, density=True, range=(rmin, rmax), weights=w3)
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="blue",
        label=leg2,
        ax=ax,
    )

    hep.histplot(
        hist3,
        bins=bins3,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        linestyle="--",
        label=leg3,
        ax=ax,
    )

    region = "eta"+str(eta_range) + "__pt"+str(pt_range)
    region = region.replace(" ","")
    region = region.replace(".","p")
    region= region.replace("[","_")
    region = region.replace("]","")
    region = region.replace(",","to") 
    #print(region)

    if not os.path.exists(outpath): os.mkdir(outpath)
    if not os.path.exists(outpath+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", outpath+"index.php")
    if not os.path.exists(outpath+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", outpath+"res")

    sub_folder = outpath+"1D_dist_log/"
    if not os.path.exists(sub_folder): os.mkdir(sub_folder)
    if not os.path.exists(sub_folder+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", sub_folder+"index.php")
    if not os.path.exists(sub_folder+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", sub_folder+"res")

    path_to_save = "%s/%s/"%(sub_folder, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)
    if not os.path.exists(path_to_save+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", path_to_save+"index.php")
    if not os.path.exists(path_to_save+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res", path_to_save+"res")

    ax.set_xlim((rmin, rmax))

    ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    ax.set_ylabel(ylable, fontsize=18)
    #ax.set_xlim(rmin, rmax)

    t = r"%s$\leq$|$\eta$|<%s    %s GeV$\leq$$p_T$ <%s GeV"%(eta_range[0],eta_range[1],pt_range[0],pt_range[1])

    if pt_range[1]==999999: t = r"%s$\leq$|$\eta$|<%s    $p_T\geq$%s GeV"%(eta_range[0],eta_range[1],pt_range[0])
    
    ax.set_title(t)
    
    ax.legend(fontsize=15)
    ax.set_yscale('log')
    fig.savefig("%s%s.png"%(path_to_save, xlable))
    fig.savefig("%s%s.pdf"%(path_to_save, xlable))
    fig.clf()



def relChIso(df_):
    return df_.chargedHadronIso/df_.pt

def relNeuIso(df_):
    return df_.neutralHadronIso/df_.pt

def relPhIso(df_):
    return df_.photonIso/df_.pt

def pt_by_genpt(df_):
    return df_.pt/df_.genpt



def idx_array(a, var_nbins, var_range):

    return np.array( var_nbins*(a-var_range[0]) / (var_range[1]-var_range[0]), dtype=np.int8 )


def event_weights(ref, samp1, samp2):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)

    samp2_weights = np.ones_like(samp2.pt.values)
    samp2_weights = samp2_weights/np.sum(samp2_weights)

    pt_range = [40, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]
    r9_range = [0.1, 1.2]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8
    r9_bins = 25

    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values, samp1.eta.values, samp1.phi.values, samp1.r9.values), bins = (pt_bins, eta_bins, phi_bins, r9_bins), range = [pt_range, eta_range, phi_range, r9_range], weights = samp1_weights)
    samp2_histo, edges = np.histogramdd(sample = (samp2.pt.values, samp2.eta.values, samp2.phi.values, samp2.r9.values), bins = (pt_bins, eta_bins, phi_bins, r9_bins), range = [pt_range, eta_range, phi_range, r9_range], weights = samp2_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values, ref.eta.values, ref.phi.values, ref.r9.values), bins = (pt_bins, eta_bins, phi_bins, r9_bins), range = [pt_range, eta_range, phi_range, r9_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1
    samp1_r9_idx = np.digitize(samp1.r9.values, edges[3])-1

    samp2_pt_idx = np.digitize(samp2.pt.values, edges[0])-1
    samp2_eta_idx = np.digitize(samp2.eta.values, edges[1])-1
    samp2_phi_idx = np.digitize(samp2.phi.values, edges[2])-1
    samp2_r9_idx = np.digitize(samp2.r9.values, edges[3])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_r9_idx]/(samp1_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_r9_idx]+1e-10))
    samp2_weights = samp2_weights*(ref_histo[samp2_pt_idx, samp2_eta_idx, samp2_phi_idx, samp2_r9_idx]/samp2_histo[samp2_pt_idx, samp2_eta_idx, samp2_phi_idx, samp2_r9_idx]+1e-10)

    return ref_weights, samp1_weights, samp2_weights


"""def event_weights_dummy(ref, samp1, samp2):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)

    samp2_weights = np.ones_like(samp2.pt.values)
    samp2_weights = samp2_weights/np.sum(samp2_weights)

    pt_range = [40, 300]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]

    pt_bins = 50
    eta_bins = 30
    phi_bins = 10

    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values), bins = (pt_bins), range = [pt_range], weights = samp1_weights)
    samp2_histo, edges = np.histogramdd(sample = (samp2.pt.values), bins = (pt_bins), range = [pt_range], weights = samp2_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values), bins = (pt_bins), range = [pt_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    #samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    #samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1

    samp2_pt_idx = np.digitize(samp2.pt.values, edges[0])-1
    #samp2_eta_idx = np.digitize(samp2.eta.values, edges[1])-1
    #samp2_phi_idx = np.digitize(samp2.phi.values, edges[2])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx]/(samp1_histo[samp1_pt_idx]+1e-10))
    samp2_weights = samp2_weights*(ref_histo[samp2_pt_idx]/samp2_histo[samp2_pt_idx]+1e-10)

    return ref_weights, samp1_weights, samp2_weights"""




    













xlable_keys = {"r9": r"$5 \times 5$ $R_9$",
    "sieie": r"$5 \times 5$ $\sigma_{i\eta i\eta}$",
    "chargedHadronIso": r"Charged hadron isolation [GeV]",
    "neutralHadronIso": r"Neutral hadron isolation [GeV]",
    "photonIso": r"Photon isolation [GeV]",
    "relchargedHadronIso": r"Rel. charged hadron isolation",
    "relneutralHadronIso": r"Rel. neutral hadron isolation",
    "relphotonIso": r"Rel. photon isolation",
    "hadronicOverEm": r"H/E",
    "pt": r"$p_T$ [GeV]",
    "eta": r"$\eta$",
    "phi": r"$\phi$ [rad]",
    "trkIso": r"Track isolation [GeV]",
    "ecalIso": r"ECAL PF cluster isolation [GeV]",
    "hcalIso": r"HCAL PF cluster isolation [GeV]"
    }


pt_range_ = [40, 200]
eta_range_ = [-2.5, 2.5]
phi_range_ = [-3.2, 3.2]
r9_range_ = [0.1, 1.2]


df = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/photon_GJets_30files.pickle")#.head(200000)
print("\n \t file1_read")

#df["relneutralHadronIso"] = df.apply(relNeuIso, axis=1)
#df["relphotonIso"] = df.apply(relPhIso, axis=1)
#df["relchargedHadronIso"] = df.apply(relChIso, axis=1)
#df["pt_by_genpt"] = df.apply(pt_by_genpt, axis=1)

df_real_pho_Gjet = df.loc[(df.is_real==1) & (df.pt>pt_range_[0]) & (df.pt<pt_range_[1]) & (df.eta>eta_range_[0]) & (df.eta<eta_range_[1]) & (df.phi>phi_range_[0]) & (df.phi<phi_range_[1]) & (df.r9>r9_range_[0]) & (df.r9<r9_range_[1])]
df_fake_pho_Gjet = df.loc[(df.is_real==0) & (df.pt>pt_range_[0]) & (df.pt<pt_range_[1]) & (df.eta>eta_range_[0]) & (df.eta<eta_range_[1]) & (df.phi>phi_range_[0]) & (df.phi<phi_range_[1]) & (df.r9>r9_range_[0]) & (df.r9<r9_range_[1])]

print("\t", r"Num. GJets real photons: ", df_real_pho_Gjet.pt.values.size)
print("\t", r"Num. GJets fake photons: ", df_fake_pho_Gjet.pt.values.size)

print("\t file1_complete", "\n")



df_fake_pho_DY = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/photon_DY_30files.pickle")#.head(100000)
print("\t file2_read")

#df2["relchargedHadronIso"] = df2.apply(relChIso, axis=1)
#df2["relneutralHadronIso"] = df2.apply(relNeuIso, axis=1)
#df2["relphotonIso"] = df2.apply(relPhIso, axis=1)
#df2["pt_by_genpt"] = df2.apply(pt_by_genpt, axis=1)
#

df_fake_pho_DY = df_fake_pho_DY.loc[(df_fake_pho_DY.pt>pt_range_[0]) & (df_fake_pho_DY.pt<pt_range_[1]) & (df_fake_pho_DY.eta>eta_range_[0]) & (df_fake_pho_DY.eta<eta_range_[1]) & (df_fake_pho_DY.phi>phi_range_[0]) & (df_fake_pho_DY.phi<phi_range_[1]) & (df_fake_pho_DY.r9>r9_range_[0]) & (df_fake_pho_DY.r9<r9_range_[1])]

print("\t", r"Num. DYto2E fake photons: ", df_fake_pho_DY.pt.values.size)

print("\t file2_complete","\n")


print("\t getting event weights....")
real_pho_Gjet_w, fake_pho_Gjet_w, fake_pho_DY_w = event_weights(df_real_pho_Gjet, df_fake_pho_Gjet, df_fake_pho_DY)

df_real_pho_Gjet["weights"] = real_pho_Gjet_w
df_fake_pho_Gjet["weights"] = fake_pho_Gjet_w
df_fake_pho_DY["weights"] = fake_pho_DY_w

print("\t event weights done \n")



#eta_bin = [0, 2.5]
#pt_bin = [40, 200]
eta_bin = [0, 0.7, 1.4442, 1.566, 2.0, 2.5]
pt_bin = [40, 70, 120, 200]


out_path = "/eos/user/n/nkasarag/www/folder/non_isolated_photonID/Photons_reweight_pt_eta_phi_r9_30files_/"

for eta in range(len(eta_bin)-1):

    if eta_bin[eta]==1.4442: continue

    for pt in range(len(pt_bin)-1):

        df_sub_1 = df_real_pho_Gjet.loc[(df_real_pho_Gjet.pt>pt_bin[pt]) & (df_real_pho_Gjet.pt<pt_bin[pt+1]) & (abs(df_real_pho_Gjet.eta)>eta_bin[eta]) & (abs(df_real_pho_Gjet.eta)<eta_bin[eta+1]) & (df_real_pho_Gjet.is_real==1)]

        df_sub_2 = df_fake_pho_Gjet.loc[(df_fake_pho_Gjet.pt>pt_bin[pt]) & (df_fake_pho_Gjet.pt<pt_bin[pt+1]) & (abs(df_fake_pho_Gjet.eta)>eta_bin[eta]) & (abs(df_fake_pho_Gjet.eta)<eta_bin[eta+1]) & (df_fake_pho_Gjet.is_real==0)]

        df_sub_3 = df_fake_pho_DY.loc[(df_fake_pho_DY.pt>pt_bin[pt]) & (df_fake_pho_DY.pt<pt_bin[pt+1]) & (abs(df_fake_pho_DY.eta)>eta_bin[eta]) & (abs(df_fake_pho_DY.eta)<eta_bin[eta+1])]

        eta_range_plot = [eta_bin[eta],eta_bin[eta+1]]
        pt_range_plot = [pt_bin[pt],pt_bin[pt+1]]

        print("\n", eta_range_plot, pt_range_plot)

        for key in xlable_keys.keys():

            
            val_1 = df_sub_1[key].values
            val_2 = df_sub_2[key].values
            val_3 = df_sub_3[key].values

            w_1 = df_sub_1["weights"].values
            w_2 = df_sub_2["weights"].values
            w_3 = df_sub_3["weights"].values

            plot_hist_log(val_1, val_2, val_3, w_1, w_2, w_3, r"GJets real $\gamma$", r"GJets fake $\gamma$", r"DYto2E fake $\gamma$", key, "Normalized entries", out_path, eta_range_plot, pt_range_plot)
            plot_hist(val_1, val_2, val_3, w_1, w_2, w_3, r"GJets real $\gamma$", r"GJets fake $\gamma$", r"DYto2E fake $\gamma$", key, "Normalized entries", out_path, eta_range_plot, pt_range_plot)

            print(eta_range_plot, pt_range_plot, key)



