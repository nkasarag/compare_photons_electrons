from unittest import TextTestRunner
import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
#hep.style.use(hep.style.ROOT)
hep.style.use("CMS")
import os
from hist import Hist

import pandas as pd
import shutil





def plot_hist(lst1, lst3, w1, w3, leg1, leg3, xlable, ylable, outpath, eta_range, pt_range):

    #nbins=50

    range_keys = {
    "energyRaw": [0, 200 if eta_range[1]<1.5 else 400, 80],
    "r9":  [0.4, 1.2, 80],
    "sieie": [0, 0.02 if eta_range[1]<1.5 else 0.06, 80],
    "etaWidth": [0, 0.021 if eta_range[1]<1.5 else 0.028, 80],
    "phiWidth": [0, 0.05 if eta_range[1]<1.5 else 0.1, 80],
    "sieip": [0, 0.003 if eta_range[1]<1.5 else 0.008, 40],
    "s4": [0.4, 1.2, 80],
    "hoe": [0, 0.5, 40],
    "ecalPFClusterIso": [0, 10, 40],
    "trkSumPtHollowConeDR03": [0, 10, 40],
    "trkSumPtSolidConeDR04": [0, 10, 40],
    "pfChargedIso": [0, 10, 40],
    "pfChargedIsoWorstVtx": [0, 10, 40],
    "esEffSigmaRR": [0, 15, 40],
    "esEnergyOverRawE": [0, 2, 40],
    "hcalPFClusterIso": [0, 10, 40],
    "pt": [20, 200, 25],
    "eta": [-2.5, 2.5, 20],
    "phi": [-3.2, 3.2, 8],
    "rho": [0, 50, 16],
    "PhotonMVAEstimatorRunIIFall17v2Values": [0.2, 1, 80]
    }

    rmin = range_keys[xlable][0]
    rmax = range_keys[xlable][1]
    nbins = range_keys[xlable][2]



    hist1, bins1 = np.histogram(lst1, bins=nbins, range=(rmin, rmax), weights=w1)#, density=True)
    hist3, bins3 = np.histogram(lst3, bins=nbins, range=(rmin, rmax), weights=w3)#, density=True)

    hist1_int = np.sum(hist1)*(bins1[1]-bins1[0])
    hist3_int = np.sum(hist3)*(bins3[1]-bins3[0])

    hist1 = hist1/hist1_int
    hist3 = hist3/hist3_int


    hist1_err, _ = np.histogram(lst1, bins=nbins, range=(rmin, rmax), weights=(w1*w1))
    hist3_err, _ = np.histogram(lst3, bins=nbins, range=(rmin, rmax), weights=(w3*w3))
#   

    hist1_err = np.sqrt(hist1_err)/hist1_int
    hist3_err = np.sqrt(hist3_err)/hist3_int


    
    
    #fig, ax = plt.subplots(figsize=(8, 7))
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]}, sharex=True, figsize=(8, 9))
    hep.histplot(
        hist1,
        bins=bins1,
        yerr = hist1_err,
        #histtype="errorbar",
        color="black",
        alpha=1,
        #edgecolor="black",
        linewidth=2,
        label=leg1,
        ax=ax[0],
    )

    hep.histplot(
        hist3,
        bins=bins3,
        yerr = hist3_err,
        #histtype="errorbar",
        color="red",
        alpha=1,
        #edgecolor="red",
        linewidth=2,
        #linestyle="--",
        label=leg3,
        ax=ax[0],
    )

    ratio = np.nan_to_num(hist3/hist1)
    ratio_err = np.nan_to_num(ratio*np.sqrt((hist3_err/hist3)**2+(hist1_err/hist1)**2))
    

    hep.histplot(
        ratio,
        bins=bins3,
        yerr = ratio_err,
        histtype="errorbar",
        color="red",
        alpha=1,
        #edgecolor="red",
        #linestyle="--",
        ax=ax[1],
    )

    

    region = "eta"+str(eta_range) + "__pt"+str(pt_range)
    region = region.replace(" ","")
    region = region.replace(".","p")
    region= region.replace("[","_")
    region = region.replace("]","")
    region = region.replace(",","to")
    #print(region)

    if not os.path.exists(outpath): os.mkdir(outpath)
    if not os.path.exists(outpath+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", outpath+"index.php")
    if not os.path.exists(outpath+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", outpath+"res")

    sub_folder = outpath+"1D_dist/"
    if not os.path.exists(sub_folder): os.mkdir(sub_folder)
    if not os.path.exists(sub_folder+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", sub_folder+"index.php")
    if not os.path.exists(sub_folder+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", sub_folder+"res")

    path_to_save = "%s/%s/"%(sub_folder, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)
    if not os.path.exists(path_to_save+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", path_to_save+"index.php")
    if not os.path.exists(path_to_save+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", path_to_save+"res")

 
    ax[0].set_ylim(0, 1.3*ax[0].get_ylim()[1])
    ax[1].set_ylim(0.5, 1.5)
    ax[0].set_xlim((rmin, rmax))

    ax[1].axhline(1, 0, 1, label=None, linestyle='--', color="black", linewidth=1)

    if eta_range[0]>=0 and eta_range[1]<1.5: ax[1].set_xlabel(xlable_keys[xlable]+" (EB)", fontsize=22)
    elif eta_range[0]>1.5 and eta_range[1]<=2.5: ax[1].set_xlabel(xlable_keys[xlable]+" (EE)", fontsize=22)
    else: ax[1].set_xlabel(xlable_keys[xlable], fontsize=22)

    ax[1].set_ylabel("Ratio", fontsize=18)
    ax[0].set_ylabel(ylable, fontsize=22)
    #ax.set_xlim(rmin, rmax)

    t = r"%s$\leq$|$\eta$|<%s    %s GeV$\leq$$p_T$ <%s GeV"%(eta_range[0],eta_range[1],pt_range[0],pt_range[1])

    if pt_range[1]==999999: t = r"%s$\leq$|$\eta$|<%s    $p_T\geq$%s GeV"%(eta_range[0],eta_range[1],pt_range[0])
    
    hep.cms.label(data=True, label="Private Work",ax=ax[0],  year=2018, com=13, fontsize=18, loc=0)

    #ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
    #ax.set_title(t)
    fig.subplots_adjust(left=0.15, right=0.95)
    #plt.tight_layout()
    ax[0].legend(fontsize=18)
    fig.savefig("%s%s.png"%(path_to_save, xlable))
    fig.savefig("%s%s.pdf"%(path_to_save, xlable))
    fig.clf()



def plot_hist_log(lst1, lst3, w1, w3, leg1, leg3, xlable, ylable, outpath, eta_range, pt_range):

    #nbins=50

    range_keys = {
    "energyRaw": [0, 200 if eta_range[1]<1.5 else 400, 80],
    "r9":  [0.4, 1.2, 80],
    "sieie": [0, 0.02 if eta_range[1]<1.5 else 0.06, 80],
    "etaWidth": [0, 0.021 if eta_range[1]<1.5 else 0.028, 80],
    "phiWidth": [0, 0.05 if eta_range[1]<1.5 else 0.1, 80],
    "sieip": [0, 0.003 if eta_range[1]<1.5 else 0.008, 40],
    "s4": [0.4, 1.2, 80],
    "hoe": [0, 0.5, 40],
    "ecalPFClusterIso": [0, 10, 40],
    "trkSumPtHollowConeDR03": [0, 10, 40],
    "trkSumPtSolidConeDR04": [0, 10, 40],
    "pfChargedIso": [0, 10, 40],
    "pfChargedIsoWorstVtx": [0, 10, 40],
    "esEffSigmaRR": [0, 15, 40],
    "esEnergyOverRawE": [0, 2, 40],
    "hcalPFClusterIso": [0, 10, 40],
    "pt": [20, 200, 25],
    "eta": [-2.5, 2.5, 20],
    "phi": [-3.2, 3.2, 8],
    "rho": [0, 50, 16],
    "PhotonMVAEstimatorRunIIFall17v2Values": [0.2, 1, 80]
    }

    rmin = range_keys[xlable][0]
    rmax = range_keys[xlable][1]
    nbins = range_keys[xlable][2]



    hist1, bins1 = np.histogram(lst1, bins=nbins, range=(rmin, rmax), weights=w1)#, density=True)
    hist3, bins3 = np.histogram(lst3, bins=nbins, range=(rmin, rmax), weights=w3)#, density=True)

    hist1_int = np.sum(hist1)*(bins1[1]-bins1[0])
    hist3_int = np.sum(hist3)*(bins3[1]-bins3[0])

    hist1 = hist1/hist1_int
    hist3 = hist3/hist3_int


    hist1_err, _ = np.histogram(lst1, bins=nbins, range=(rmin, rmax), weights=(w1*w1))
    hist3_err, _ = np.histogram(lst3, bins=nbins, range=(rmin, rmax), weights=(w3*w3))
#   

    hist1_err = np.sqrt(hist1_err)/hist1_int
    hist3_err = np.sqrt(hist3_err)/hist3_int


    
    
    #fig, ax = plt.subplots(figsize=(8, 7))
    fig, ax = plt.subplots(2, 1, gridspec_kw={'height_ratios': [4, 1]}, figsize=(8, 9), sharex=True)
    

    ratio = np.nan_to_num(hist3/hist1)
    ratio_err = np.nan_to_num(ratio*np.sqrt((hist3_err/hist3)**2+(hist1_err/hist1)**2))

    hep.histplot(
        ratio,
        bins=bins3,
        yerr = ratio_err,
        histtype="errorbar",
        color="red",
        alpha=1,
        #edgecolor="red",
        #linestyle="--",
        ax=ax[1],
    )

    hep.histplot(
        hist1,
        bins=bins1,
        yerr = hist1_err,
        #histtype="errorbar",
        color="black",
        alpha=1,
        #edgecolor="black",
        linewidth=2,
        label=leg1,
        ax=ax[0],
    )

    hep.histplot(
        hist3,
        bins=bins3,
        yerr = hist3_err,
        #histtype="errorbar",
        color="red",
        alpha=1,
        #edgecolor="red",
        linewidth=2,
        #linestyle="--",
        label=leg3,
        ax=ax[0],
    )
    
    

    

    region = "eta"+str(eta_range) + "__pt"+str(pt_range)
    region = region.replace(" ","")
    region = region.replace(".","p")
    region= region.replace("[","_")
    region = region.replace("]","")
    region = region.replace(",","to")
    #print(region)

    if not os.path.exists(outpath): os.mkdir(outpath)
    if not os.path.exists(outpath+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", outpath+"index.php")
    if not os.path.exists(outpath+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", outpath+"res")

    sub_folder = outpath+"1D_dist_log/"
    if not os.path.exists(sub_folder): os.mkdir(sub_folder)
    if not os.path.exists(sub_folder+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", sub_folder+"index.php")
    if not os.path.exists(sub_folder+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", sub_folder+"res")

    path_to_save = "%s/%s/"%(sub_folder, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)
    if not os.path.exists(path_to_save+"index.php"): shutil.copy("/eos/user/n/nkasarag/www/folder/index.php", path_to_save+"index.php")
    if not os.path.exists(path_to_save+"res"): shutil.copytree("/eos/user/n/nkasarag/www/folder/res2", path_to_save+"res")

    ax[1].set_ylim(0.5, 1.5)
    ax[0].set_xlim((rmin, rmax))
    ax[1].axhline(1, 0, 1, label=None, linestyle='--', color="black", linewidth=1)

    if eta_range[0]>=0 and eta_range[1]<1.5: ax[1].set_xlabel(xlable_keys[xlable]+" (EB)", fontsize=22)
    elif eta_range[0]>1.5 and eta_range[1]<=2.5: ax[1].set_xlabel(xlable_keys[xlable]+" (EE)", fontsize=22)
    else: ax[1].set_xlabel(xlable_keys[xlable], fontsize=22)

    
    ax[1].set_ylabel("Ratio", fontsize=18)
    ax[0].set_ylabel(ylable, fontsize=22)
    #ax.set_xlim(rmin, rmax)

    t = r"%s$\leq$|$\eta$|<%s    %s GeV$\leq$$p_T$ <%s GeV"%(eta_range[0],eta_range[1],pt_range[0],pt_range[1])

    if pt_range[1]==999999: t = r"%s$\leq$|$\eta$|<%s    $p_T\geq$%s GeV"%(eta_range[0],eta_range[1],pt_range[0])
    
    #ax.set_title(t)
    #ax.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    hep.cms.label(data=True, label="Private Work",ax=ax[0], year=2018, com=13, fontsize=18, loc=0)

    fig.subplots_adjust(left=0.15, right=0.95)
    #plt.tight_layout()
    ax[0].set_yscale("log")
    ax[0].legend(fontsize=18)
    fig.savefig("%s%s.png"%(path_to_save, xlable))
    fig.savefig("%s%s.pdf"%(path_to_save, xlable))
    fig.clf()





def idx_array(a, var_nbins, var_range):

    return np.array( var_nbins*(a-var_range[0]) / (var_range[1]-var_range[0]), dtype=np.int8 )


def event_weights(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    pt_range = [20, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8

    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values, samp1.eta.values, samp1.phi.values), bins = (pt_bins, eta_bins, phi_bins), range = [pt_range, eta_range, phi_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values, ref.eta.values, ref.phi.values), bins = (pt_bins, eta_bins, phi_bins), range = [pt_range, eta_range, phi_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx]/(samp1_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx]+1e-10))

    return ref_weights, samp1_weights

def event_weights_r9_eta_phi_pt(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    pt_range = [20, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8

    r9_range = [0.4, 1.2]

    r9_bins = 20

    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values, samp1.eta.values, samp1.phi.values, samp1.r9.values), bins = (pt_bins, eta_bins, phi_bins, r9_bins), range = [pt_range, eta_range, phi_range, r9_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values, ref.eta.values, ref.phi.values, ref.r9.values), bins = (pt_bins, eta_bins, phi_bins, r9_bins), range = [pt_range, eta_range, phi_range, r9_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1
    samp1_r9_idx = np.digitize(samp1.r9.values, edges[3])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_r9_idx]/(samp1_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_r9_idx]+1e-10))

    return ref_weights, samp1_weights





def event_weights_pt_eta_phi_rho(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    pt_range = [20, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]
    rho_range = [0, 65]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8
    rho_bins = 20


    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values, samp1.eta.values, samp1.phi.values, samp1.rho.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins), range = [pt_range, eta_range, phi_range, rho_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values, ref.eta.values, ref.phi.values, ref.rho.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins), range = [pt_range, eta_range, phi_range, rho_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1
    samp1_rho_idx = np.digitize(samp1.rho.values, edges[3])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx]/(samp1_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx]+1e-10))

    return ref_weights, samp1_weights


def event_weights_pt_eta_phi_rho_r9(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    pt_range = [20, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]
    rho_range = [0, 65]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8
    rho_bins = 20

    r9_range = [0.2, 1.2]

    r9_bins = 15


    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values, samp1.eta.values, samp1.phi.values, samp1.rho.values, samp1.r9.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins, r9_bins), range = [pt_range, eta_range, phi_range, rho_range, r9_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values, ref.eta.values, ref.phi.values, ref.rho.values, ref.r9.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins, r9_bins), range = [pt_range, eta_range, phi_range, rho_range, r9_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1
    samp1_rho_idx = np.digitize(samp1.rho.values, edges[3])-1
    samp1_r9_idx = np.digitize(samp1.r9.values, edges[4])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx, samp1_r9_idx]/(samp1_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx, samp1_r9_idx]+1e-10))

    return ref_weights, samp1_weights


def event_weights_only_r9(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    r9_range = [0.4, 1.2]

    r9_bins = 20

    samp1_histo, edges = np.histogramdd(sample = (samp1.r9.values), bins = (r9_bins), range = [r9_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.r9.values), bins = (r9_bins), range = [r9_range], weights = ref_weights)

    samp1_r9_idx = np.digitize(samp1.r9.values, edges[0])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_r9_idx]/(samp1_histo[samp1_r9_idx]+1e-10))

    return ref_weights, samp1_weights



def event_weights_pt_eta_phi_rho_and_then_r9(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    pt_range = [20, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]
    rho_range = [0, 65]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8
    rho_bins = 20


    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values, samp1.eta.values, samp1.phi.values, samp1.rho.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins), range = [pt_range, eta_range, phi_range, rho_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values, ref.eta.values, ref.phi.values, ref.rho.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins), range = [pt_range, eta_range, phi_range, rho_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1
    samp1_rho_idx = np.digitize(samp1.rho.values, edges[3])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx]/(samp1_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx]+1e-10))

    r9_range = [0.4, 1.2]

    r9_bins = 20

    samp1_histo, edges = np.histogramdd(sample = (samp1.r9.values), bins = (r9_bins), range = [r9_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.r9.values), bins = (r9_bins), range = [r9_range], weights = ref_weights)

    samp1_r9_idx = np.digitize(samp1.r9.values, edges[0])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_r9_idx]/(samp1_histo[samp1_r9_idx]+1e-10))

    return ref_weights, samp1_weights

def event_weights_pt_eta_phi_rho_and_then_r9(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    pt_range = [20, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]
    rho_range = [0, 65]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8
    rho_bins = 20


    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values, samp1.eta.values, samp1.phi.values, samp1.rho.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins), range = [pt_range, eta_range, phi_range, rho_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values, ref.eta.values, ref.phi.values, ref.rho.values), bins = (pt_bins, eta_bins, phi_bins, rho_bins), range = [pt_range, eta_range, phi_range, rho_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[2])-1
    samp1_rho_idx = np.digitize(samp1.rho.values, edges[3])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx]/(samp1_histo[samp1_pt_idx, samp1_eta_idx, samp1_phi_idx, samp1_rho_idx]+1e-10))

    r9_range = [0.4, 1.2]

    r9_bins = 20

    samp1_histo_2, edges = np.histogramdd(sample = (samp1.r9.values), bins = (r9_bins), range = [r9_range], weights = samp1_weights)
    ref_histo_2, edges = np.histogramdd(sample = (ref.r9.values), bins = (r9_bins), range = [r9_range], weights = ref_weights)

    samp1_r9_idx = np.digitize(samp1.r9.values, edges[0])-1

    samp1_weights = samp1_weights*(ref_histo_2[samp1_r9_idx]/(samp1_histo_2[samp1_r9_idx]+1e-10))

    return ref_weights, samp1_weights

def event_weights_pt_phi_rho_and_then_eta_r9(ref, samp1):

    ref_weights = np.ones_like(ref.pt.values)
    ref_weights = ref_weights/np.sum(ref_weights)

    samp1_weights = np.ones_like(samp1.pt.values)
    samp1_weights = samp1_weights/np.sum(samp1_weights)



    pt_range = [20, 200]
    eta_range = [-2.5, 2.5]
    phi_range = [-3.2, 3.2]
    rho_range = [0, 65]

    pt_bins = 25
    eta_bins = 20
    phi_bins = 8
    rho_bins = 20


    samp1_histo, edges = np.histogramdd(sample = (samp1.pt.values,  samp1.phi.values, samp1.rho.values), bins = (pt_bins,  phi_bins, rho_bins), range = [pt_range,  phi_range, rho_range], weights = samp1_weights)
    ref_histo, edges = np.histogramdd(sample = (ref.pt.values,  ref.phi.values, ref.rho.values), bins = (pt_bins,  phi_bins, rho_bins), range = [pt_range, phi_range, rho_range], weights = ref_weights)


    samp1_pt_idx = np.digitize(samp1.pt.values, edges[0])-1
    
    samp1_phi_idx = np.digitize(samp1.phi.values, edges[1])-1
    samp1_rho_idx = np.digitize(samp1.rho.values, edges[2])-1

    samp1_weights = samp1_weights*(ref_histo[samp1_pt_idx, samp1_phi_idx, samp1_rho_idx]/(samp1_histo[samp1_pt_idx, samp1_phi_idx, samp1_rho_idx]+1e-10))

    r9_range = [0.4, 1.2]

    r9_bins = 20

    samp1_histo_2, edges = np.histogramdd(sample = (samp1.r9.values, samp1.eta.values), bins = (r9_bins, eta_bins), range = [r9_range, eta_range], weights = samp1_weights)
    ref_histo_2, edges = np.histogramdd(sample = (ref.r9.values, ref.eta.values), bins = (r9_bins, eta_bins), range = [r9_range, eta_range], weights = ref_weights)

    samp1_r9_idx = np.digitize(samp1.r9.values, edges[0])-1
    samp1_eta_idx = np.digitize(samp1.eta.values, edges[1])-1

    samp1_weights = samp1_weights*(ref_histo_2[samp1_r9_idx, samp1_eta_idx]/(samp1_histo_2[samp1_r9_idx, samp1_eta_idx]+1e-10))

    return ref_weights, samp1_weights







xlable_keys = {
    "energyRaw": "Raw Energy [GeV]",
    "r9":  r"$R_9$",
    "sieie": r"$\sigma_{i\eta i\eta}$",
    "etaWidth": r"etaWidth",
    "phiWidth": r"phiWidth [rad]",
    "sieip": r"$\sigma_{i\eta i\phi}$",
    "s4": r"$S_4$",
    "hoe": r"H/E",
    "ecalPFClusterIso": r"ECAL PF cluster isolation [GeV]",
    "trkSumPtHollowConeDR03": "trkSumPtHollowConeDR03 [GeV]",
    "trkSumPtSolidConeDR04": "trkSumPtSolidConeDR04 [GeV]",
    "pfChargedIso": r"pfChargedIso [GeV]",
    "pfChargedIsoWorstVtx": r"pfChargedIsoWorstVtx [GeV]",
    "esEffSigmaRR": "esEffSigmaRR",
    "esEnergyOverRawE": "esEnergyOverRawE",
    "hcalPFClusterIso": r"HCAL PF cluster isolation [GeV]",
    "pt": r"$p_T$ [GeV]",
    "eta": r"$\eta$",
    "phi": r"$\phi$ [rad]",
    "rho": r"$\rho$",
    "PhotonMVAEstimatorRunIIFall17v2Values": r"PhotonMVAEstimatorRunIIFall17v2Values"
    }



pt_range_ = [20, 200]
eta_range_ = [-2.5, 2.5]
phi_range_ = [-3.2, 3.2]
r9_range_ = [0.4, 1.2]
rho_range_ = [0, 65]


#df = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/unconverted_photon_for_NFlow.pickle")#.head(1000)
#df = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/unconverted_photon_for_NFlow_withRho_MVA.pickle")
df = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/photon_for_NFlow_withRho_MVA_conv_info.pickle")
print("\n \t file1_read")



df_real_pho_Gjet = df.loc[(df.is_photon==1) & (df.pt>pt_range_[0]) & (df.pt<pt_range_[1]) & (df.eta>eta_range_[0]) & (df.eta<eta_range_[1]) & (df.phi>phi_range_[0]) & (df.phi<phi_range_[1]) & (df.rho>rho_range_[0]) & (df.rho<rho_range_[1])]
#df_real_pho_Gjet = df_real_pho_Gjet.loc[(df_real_pho_Gjet.r9>r9_range_[0]) & (df_real_pho_Gjet.r9<r9_range_[1])]

print("\t", r"Num. GJets real photons: ", df_real_pho_Gjet.pt.values.size)

print("\t file1_complete", "\n")



#df_fake_pho_DY = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/ele_fPhoton_for_NFlow.pickle")#.head(1000)
df_fake_pho_DY = pd.read_pickle("/eos/user/n/nkasarag/non_isolated_photon_id/ele_fPhoton_for_NFlow_withRho_MVA.pickle")#.head(1000)
print("\t file2_read")



df_fake_pho_DY = df_fake_pho_DY.loc[(df_fake_pho_DY.is_photon==0) & (df_fake_pho_DY.pt>pt_range_[0]) & (df_fake_pho_DY.pt<pt_range_[1]) & (df_fake_pho_DY.eta>eta_range_[0]) & (df_fake_pho_DY.eta<eta_range_[1]) & (df_fake_pho_DY.phi>phi_range_[0]) & (df_fake_pho_DY.phi<phi_range_[1]) & (df_fake_pho_DY.rho>rho_range_[0]) & (df_fake_pho_DY.rho<rho_range_[1])]
#df_fake_pho_DY = df_fake_pho_DY.loc[(df_fake_pho_DY.r9>r9_range_[0]) & (df_fake_pho_DY.r9<r9_range_[1])]

print("\t", r"Num. DYto2E fake photons: ", df_fake_pho_DY.pt.values.size)

print("\t file2_complete","\n")





print("\t getting event weights....")
#real_pho_Gjet_w, fake_pho_DY_w = event_weights(df_real_pho_Gjet, df_fake_pho_DY)
real_pho_Gjet_w, fake_pho_DY_w = event_weights_pt_eta_phi_rho(df_real_pho_Gjet, df_fake_pho_DY)
#real_pho_Gjet_w, fake_pho_DY_w = event_weights_pt_eta_phi_rho_r9(df_real_pho_Gjet, df_fake_pho_DY)
#real_pho_Gjet_w, fake_pho_DY_w = event_weights_only_r9(df_real_pho_Gjet, df_fake_pho_DY)
#real_pho_Gjet_w, fake_pho_DY_w = event_weights_r9_eta_phi_pt(df_real_pho_Gjet, df_fake_pho_DY)
#real_pho_Gjet_w, fake_pho_DY_w = 1, 1
#real_pho_Gjet_w, fake_pho_DY_w = event_weights_pt_eta_phi_rho_and_then_r9(df_real_pho_Gjet, df_fake_pho_DY)
#real_pho_Gjet_w, fake_pho_DY_w = event_weights_pt_phi_rho_and_then_eta_r9(df_real_pho_Gjet, df_fake_pho_DY)




df_real_pho_Gjet["weights"] = real_pho_Gjet_w
df_fake_pho_DY["weights"] = fake_pho_DY_w

print(df_fake_pho_DY["weights"])

print("\t event weights done \n")

print(df_real_pho_Gjet.head())
print(df_fake_pho_DY.head())


#df_to_save = pd.concat([df_real_pho_Gjet, df_fake_pho_DY])

#print(df_to_save)
#print(df_to_save[["esEffSigmaRR", "esEnergyOverRawE"]])


path = "/eos/user/n/nkasarag/comp_pho_ele/"
filename = "df_for_NFlow_withRho"
#df_to_save.to_pickle("%s%s.pickle"%(path, filename))

#eta_bin = [0, 1.4442, 1.566, 2.5]
#eta_bin = [0, 2.5]
#pt_bin = [40, 100, 200]
eta_bin = [0, 1.4442, 1.566, 2.5]
pt_bin = [20, 200]

#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/all_no_reweight_werrb_wratio/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/EB_EE_reweight_pt_eta_phi_rho/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/all_reweight_pt_eta_phi_rho/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/all_reweight_pt_eta_phi_rho_r9/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/all_reweight_pt_eta_phi_rho_werrb/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/EB_EE_reweight_pt_eta_phi_rho_werrb/"

#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/EB_EE_reweight_pt_eta_phi_rho_werrb_wratio/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/all_reweight_pt_eta_phi_rho_werrb_wratio/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/all_reweight_pt_eta_phi_rho_r9_werrb_wratio/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/EB_EE_reweight_pt_eta_phi_rho_werrb_wratio/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/all_reweight_r9_werrb_wratio/"
#out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/EB_EE_reweight_pt_eta_phi_rho_r9_werrb_wratio/"

out_path = "/eos/user/n/nkasarag/www/folder/comp_pho_ele/EB_EE_reweight_pt_eta_phi_rho_werrb_wratio_all_photon/"

for eta in range(len(eta_bin)-1):

    if eta_bin[eta]==1.4442: continue

    

    for pt in range(len(pt_bin)-1):

        df_sub_1 = df_real_pho_Gjet.loc[(df_real_pho_Gjet.pt>pt_bin[pt]) & (df_real_pho_Gjet.pt<pt_bin[pt+1]) & (abs(df_real_pho_Gjet.eta)>eta_bin[eta]) & (abs(df_real_pho_Gjet.eta)<eta_bin[eta+1]) & (df_real_pho_Gjet.is_photon==1)]

        df_sub_3 = df_fake_pho_DY.loc[(df_fake_pho_DY.pt>pt_bin[pt]) & (df_fake_pho_DY.pt<pt_bin[pt+1]) & (abs(df_fake_pho_DY.eta)>eta_bin[eta]) & (abs(df_fake_pho_DY.eta)<eta_bin[eta+1])]

        eta_range_plot = [eta_bin[eta],eta_bin[eta+1]]
        pt_range_plot = [pt_bin[pt],pt_bin[pt+1]]

        print("\n", eta_range_plot, pt_range_plot)

        for key in xlable_keys.keys():

            
            val_1 = df_sub_1[key].values
            val_3 = df_sub_3[key].values

            w_1 = df_sub_1["weights"].values
            w_3 = df_sub_3["weights"].values

            #plot_hist_log(val_1, val_3, w_1, w_3, r"GluGluHToGG real $\gamma$", r"GluGluHToEE e misidentified as $\gamma$", key, "Normalized entries", out_path, eta_range_plot, pt_range_plot)
            #plot_hist(val_1, val_3, w_1, w_3, r"GluGluHToGG real $\gamma$", r"GluGluHToEE e misidentified as $\gamma$", key, "Normalized entries", out_path, eta_range_plot, pt_range_plot)

            #plot_hist_log(val_1, val_3, w_1, w_3, r"Unconverted photons (H $\rightarrow$ $\gamma\gamma$)", r"Electrons (H $\rightarrow$ ee)", key, "Normalized to unit area", out_path, eta_range_plot, pt_range_plot)
            #plot_hist(val_1, val_3, w_1, w_3, r"Unconverted photons (H $\rightarrow$ $\gamma\gamma$)", r"Electrons (H $\rightarrow$ ee)", key, "Normalized to unit area", out_path, eta_range_plot, pt_range_plot)

            plot_hist_log(val_1, val_3, w_1, w_3, r"Photons (H $\rightarrow$ $\gamma\gamma$)", r"Electrons (H $\rightarrow$ ee)", key, "Normalized to unit area", out_path, eta_range_plot, pt_range_plot)
            plot_hist(val_1, val_3, w_1, w_3, r"Photons (H $\rightarrow$ $\gamma\gamma$)", r"Electrons (H $\rightarrow$ ee)", key, "Normalized to unit area", out_path, eta_range_plot, pt_range_plot)
            
            

            print(eta_range_plot, pt_range_plot, key)



