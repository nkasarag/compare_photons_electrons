from CRABClient.UserUtilities import config

config = config()

config.General.requestName = 'Slimming_photon_DY_samples'
config.General.workArea = 'crab_projects'
config.General.transferOutputs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'slim_MiniAODs.py'

config.Data.inputDataset = '/DYto2E_M-50_NNPDF31_TuneCP5_13p6TeV-powheg-pythia8/Run3Summer22EEMiniAODv3-124X_mcRun3_2022_realistic_postEE_v1-v3/MINIAODSIM'
config.Data.inputDBS = 'global'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 10
config.Data.publication = False 
config.Data.allowNonValidInputDataset  = True
config.Data.outputDatasetTag = 'DYto2E_M-50'


config.Data.outLFNDirBase = "/store/group/phys_egamma/ec/nkasarag/DYto2E_M-50_photon_object/Pt-50toInf/"  #points to /eos/cms/store/group/phys_egamma/...
config.Data.publication = False
config.Site.storageSite = 'T2_CH_CERN'
config.Site.ignoreGlobalBlacklist = True
