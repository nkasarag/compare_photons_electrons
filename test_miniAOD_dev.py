import ROOT
ROOT.gROOT.SetBatch(True)
import numpy as np

# load FWLite C++ libraries
ROOT.gSystem.Load("libFWCoreFWLite.so");
ROOT.gSystem.Load("libDataFormatsFWLite.so");
ROOT.FWLiteEnabler.enable()
from DataFormats.FWLite import Handle, Events

#events = Events("root://xrootd-cms.infn.it///store/mc/Run3Winter22MiniAOD/GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8/MINIAODSIM/FlatPU0to70_122X_mcRun3_2021_realistic_v9-v2/2430000/0176f78e-294f-4ec1-9e8d-ddcbf471ae65.root")

#events =  Events("/eos/cms/store/group/phys_egamma/ec/fmausolf/GJet_Run3_MiniAOD/Pt-40toInf/GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8/GJetRun3_Pt-40toInf/221228_143544/0000/output_2.root")





###plot histogram

import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)

import os

def plot_hist(lst1, lst2, leg1, leg2, xlable, ylable, outpath, region):

    nbins=100

    rmax_keys = {"r9": 2,
    "sieie": min(max(max(lst1),max(lst2)), 0.08),
    "chargedHadronIso": 80,
    "neutralHadronIso": 30,
    "relchargedHadronIso": 1.5,
    "relneutralHadronIso": 0.8,
    "hadronicOverEm": 0.6,
    "pt": 300
    }

    rmax = rmax_keys[xlable]



    rmin = min(min(lst1),min(lst2))

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax))
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        label=leg2,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all": ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    else: ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)


    ax.set_ylabel(ylable, fontsize=18)
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s_%s.png"%(path_to_save, xlable, region))
    fig.savefig("%s%s_%s.pdf"%(path_to_save, xlable, region))
    fig.clf()


def plot_hist_log(lst1, lst2, leg1, leg2, xlable, ylable, outpath, region):

    nbins=100

    rmin = min(min(lst1),min(lst2))
    rmax = max(max(lst1),max(lst2))

    if xlable=="sieie": rmax = min(max(max(lst1),max(lst2)), 0.08)

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax))
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        label=leg2,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s_log/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all": ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    else: ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)

    
    ax.set_ylabel(ylable, fontsize=18)
    ax.set_yscale('log')
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s_%s_log.png"%(path_to_save, xlable, region))
    fig.savefig("%s%s_%s_log.pdf"%(path_to_save, xlable, region))
    fig.clf()






def hadronic_fake_candidate(photon_candidate, genJets, genParticles):
    """
    This function checks on truth level if the photon candidate stems from a jet.
    Loops through all genJets and genParticles and checks if
    the closest object at generator level is a prompt photon, prompt electron, prompt muon or stems from a jet.
    Returns True / False
    """

    min_DeltaR = float(99999)

    photon_vector = ROOT.TLorentzVector()
    photon_vector.SetPtEtaPhiE(photon_candidate.pt(), photon_candidate.eta(), photon_candidate.phi(), photon_candidate.energy())

    # print("\t\t Photon gen particle PDG ID:", photon_candidate.genParticle().pdgId())

    # this jet loop might be not needed... check later, but doesn't harm at this point
    jet_around_photon = False
    for genJet in genJets:
        # build four-vector to calculate DeltaR to photon
        genJet_vector = ROOT.TLorentzVector()
        genJet_vector.SetPtEtaPhiE(genJet.pt(), genJet.eta(), genJet.phi(), genJet.energy())

        DeltaR = photon_vector.DeltaR(genJet_vector)
        # print("\t\t INFO: gen jet eta, phi, delta R ", genJet.eta(), genJet.phi(), DeltaR)
        if DeltaR < 0.3:
            jet_around_photon = True

    is_prompt = False
    pdgId = 0

    for genParticle in genParticles:

        if genParticle.pt() < 1: continue # threshold of 1GeV for interesting particles

        # build four-vector to calculate DeltaR to photon
        genParticle_vector = ROOT.TLorentzVector()
        genParticle_vector.SetPtEtaPhiE(genParticle.pt(), genParticle.eta(), genParticle.phi(), genParticle.energy())
        
        DeltaR = photon_vector.DeltaR(genParticle_vector)
        # print("\t\t INFO: gen particle eta, phi, delta R ", genParticle.eta(), genParticle.phi(), DeltaR)
        if DeltaR < min_DeltaR and DeltaR < 0.3:
            min_DeltaR = DeltaR
            pdgId = genParticle.pdgId()
            is_prompt = genParticle.isPromptFinalState()
            
    #print("\t pdgId=",pdgId)
    #print("\t is_prompt=",is_prompt)
        # print("\t\t INFO: PDG ID:", pdgId)

    prompt_electron = True if (abs(pdgId)==11 and is_prompt) else False
    prompt_photon = True if (pdgId==22 and is_prompt) else False
    prompt_muon = True if (abs(pdgId)==13 and is_prompt) else False
    
    #print("\t prompt_electron=",prompt_electron)
    #print("\t prompt_photon=",prompt_photon)
    #print("\t prompt_muon=",prompt_muon)
    
    if jet_around_photon and not (prompt_electron or prompt_photon or prompt_muon):
        return True
    else:
        return False






xlable_keys = {"r9": r"$5 \times 5$ $R_9$",
    "sieie": r"$5 \times 5$ $\sigma_{i\eta i\eta}$",
    "chargedHadronIso": r"Charged hadron isolation [GeV]",
    "neutralHadronIso": r"Neutral hadron isolation [GeV]",
    "photonIso": r"Photon isolation [GeV]",
    "relchargedHadronIso": r"Rel. charged hadron isolation",
    "relneutralHadronIso": r"Rel. neutral hadron isolation",
    "relphotonIso": r"Rel. photon isolation",
    "hadronicOverEm": r"H/E",
    "pt": r"$p_T$ [GeV]",
    "eta": r"$\eta$",
    "phi": r"$\phi$"
    }



photonHandle, photonLabel = Handle("std::vector<pat::Photon>"), "slimmedPhotons"
RecHitHandle, RecHitLabel = Handle("edm::SortedCollection<EcalRecHit,edm::StrictWeakOrdering<EcalRecHit> >"), "reducedEgamma:reducedEBRecHits" 
genParticlesHandle, genParticlesLabel = Handle("std::vector<reco::GenParticle>"), "prunedGenParticles"
genJetsHandle, genJetsLabel = Handle("std::vector<reco::GenJet>"), "slimmedGenJets"



all_list_true = {"r9": [],
    "sieie": [],
    "chargedHadronIso": [],
    "neutralHadronIso": [],
    "relchargedHadronIso": [],
    "relneutralHadronIso": [],
    "hadronicOverEm": [],
    "pt": []
    }

EB_list_true = {"r9": [],
    "sieie": [],
    "chargedHadronIso": [],
    "neutralHadronIso": [],
    "relchargedHadronIso": [],
    "relneutralHadronIso": [],
    "hadronicOverEm": [],
    "pt": []
    }

EE_list_true = {"r9": [],
    "sieie": [],
    "chargedHadronIso": [],
    "neutralHadronIso": [],
    "relchargedHadronIso": [],
    "relneutralHadronIso": [],
    "hadronicOverEm": [],
    "pt": []
    }


all_list_fake = {"r9": [],
    "sieie": [],
    "chargedHadronIso": [],
    "neutralHadronIso": [],
    "relchargedHadronIso": [],
    "relneutralHadronIso": [],
    "hadronicOverEm": [],
    "pt": []
    }

EB_list_fake = {"r9": [],
    "sieie": [],
    "chargedHadronIso": [],
    "neutralHadronIso": [],
    "relchargedHadronIso": [],
    "relneutralHadronIso": [],
    "hadronicOverEm": [],
    "pt": []
    }

EE_list_fake = {"r9": [],
    "sieie": [],
    "chargedHadronIso": [],
    "neutralHadronIso": [],
    "relchargedHadronIso": [],
    "relneutralHadronIso": [],
    "hadronicOverEm": [],
    "pt": []
    }


path_GJet_files = "/eos/cms/store/group/phys_egamma/ec/nkasarag/GJet_Run3_MiniAOD/Pt-40toInf/GJet_PT-40_DoubleEMEnriched_MGG-80_TuneCP5_13p6TeV_pythia8/GJetRun3_Pt-40toInf/230828_104237/0000/"

filenames = os.listdir(path_GJet_files)
    # just take one file for tests
filenames = filenames[:1]

file_num = 0

for filename in filenames:


    file_num+=1
    print("\n \t ### processing file number: %s ###"%str(file_num))

    events = Events(path_GJet_files+filename)



    stop_index = 5
    for i,event in enumerate(events):

            if i == stop_index: 
                break 
        
            if i % 1000 == 0:
                print("\t \t INFO: processing event", i)

            event.getByLabel(photonLabel, photonHandle)
            event.getByLabel(RecHitLabel, RecHitHandle)
            event.getByLabel(genParticlesLabel, genParticlesHandle)
            event.getByLabel(genJetsLabel, genJetsHandle)

            genJets = genJetsHandle.product()
            genParticles = genParticlesHandle.product()
        
            #print(i, " genJets: ", genJets)
            #print(i, " genParticles: ", genParticles)


            #print("\n \t event =",i+1)
            
            for photon in photonHandle.product():
                #print("\t et = %s"%photon.et())
                #print("\t pt = %s"%photon.pt())
                #print("\t SC pt = %s"%(photon.superCluster().energy()*np.sin(photon.superCluster().position().theta())))
                #print("\t energy = %s"%photon.energy())
                #print("\t r9 = %s"%photon.full5x5_r9())
                #print("\t sieie = %s"%photon.full5x5_sigmaIetaIeta())
                #print("\t hadronicOverEm = %s"%photon.hadronicOverEm())
                #print("\t chargedHadronIso = %s"%photon.chargedHadronIso())
                #print("\t neutralHadronIso = %s"%photon.neutralHadronIso())
                #print("\t photonIso = %s"%photon.photonIso())
                #print("\t subdetId = %s"%photon.superCluster().seed().seed().subdetId(), "\t photonEta = %s"%photon.eta())

                is_real = False
                is_hadronic_fake = False
                if photon.pt() < 20: continue
                
                try:
                    pdgId = photon.genParticle().pdgId()
                    print("\t pt = %s"%photon.energy())
                    #print("\t SC pt = %s"%(photon.superCluster().energy()*np.sin(photon.superCluster().position().theta())))
                    print("\t gen pt = %s"%photon.genParticle().energy(), "\n")
                    if pdgId == 22:
                        is_real = True
            
                except ReferenceError:
                    is_hadronic_fake = hadronic_fake_candidate(photon, genJets, genParticles)
                    
                #print("\t jet=",hadronic_fake_candidate(photon, genJets, genParticles))  
                #print("\t is_real=",is_real)
                #print("\t is_hadronic_fake=", is_hadronic_fake)


 


out_path = "/eos/user/n/nkasarag/www/folder/non_isolated_photonID/GJet_full"



    
    
       






