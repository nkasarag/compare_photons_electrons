from CRABClient.UserUtilities import config

config = config()

config.General.requestName = 'Slimming_GluGluToEE'
config.General.workArea = 'crab_projects'
config.General.transferOutputs = True

config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'slim_MiniAODs.py'

#config.Data.inputDataset = '/GJet_PT-40_DoubleEMEnriched_MGG-80_TuneCP5_13p6TeV_pythia8/Run3Summer22EEMiniAODv3-124X_mcRun3_2022_realistic_postEE_v1-v2/MINIAODSIM'

#config.Data.inputDataset = '/GluGluHToGG_M125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v1/MINIAODSIM'
config.Data.inputDataset = '/GluGluHToEE_M125_TuneCP5_13TeV-amcatnloFXFX-pythia8/RunIISummer20UL18MiniAODv2-106X_upgrade2018_realistic_v16_L1v1-v2/MINIAODSIM'

config.Data.inputDBS = 'global'
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 10
config.Data.publication = False 
config.Data.allowNonValidInputDataset  = True
config.Data.outputDatasetTag = 'GluGluHToEE'


config.Data.outLFNDirBase = "/store/group/phys_egamma/ec/nkasarag/GluGluHToEE"  #points to /eos/cms/store/group/phys_egamma/...
config.Data.publication = False
config.Site.storageSite = 'T2_CH_CERN'
config.Site.ignoreGlobalBlacklist = True
