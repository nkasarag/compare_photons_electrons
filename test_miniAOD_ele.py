import ROOT
ROOT.gROOT.SetBatch(True)

# load FWLite C++ libraries
ROOT.gSystem.Load("libFWCoreFWLite.so");
ROOT.gSystem.Load("libDataFormatsFWLite.so");
ROOT.FWLiteEnabler.enable()
from DataFormats.FWLite import Handle, Events

#events = Events("root://xrootd-cms.infn.it///store/mc/Run3Winter22MiniAOD/GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8/MINIAODSIM/FlatPU0to70_122X_mcRun3_2021_realistic_v9-v2/2430000/0176f78e-294f-4ec1-9e8d-ddcbf471ae65.root")

#events =  Events("/eos/cms/store/group/phys_egamma/ec/fmausolf/GJet_Run3_MiniAOD/Pt-40toInf/GJet_Pt-40toInf_DoubleEMEnriched_TuneCP5_13p6TeV_pythia8/GJetRun3_Pt-40toInf/221228_143544/0000/output_2.root")





###plot histogram

import numpy as np
import matplotlib.pyplot as plt

import mplhep as hep
hep.style.use(hep.style.ROOT)

import os

def plot_hist(lst1, lst2, leg1, leg2, xlable, ylable, outpath, region):

    nbins=100

    rmax_keys = {"r9": 2,
    "sieie": min(max(max(lst1),max(lst2)), 0.08),
    "chargedHadronIso": 80,
    "neutralHadronIso": 30,
    "photonIso": 100,
    "relchargedHadronIso": 1.5,
    "relneutralHadronIso": 0.8,
    "relphotonIso": 2,
    "hadronicOverEm": 0.6,
    "pt": 300,
    "eta": 3,
    "phi": 3
    }

    rmax = rmax_keys[xlable]



    rmin = min(min(lst1),min(lst2))

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax))
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        label=leg2,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all": ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    else: ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)


    ax.set_ylabel(ylable, fontsize=18)
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s_%s.png"%(path_to_save, xlable, region))
    fig.savefig("%s%s_%s.pdf"%(path_to_save, xlable, region))
    fig.clf()


def plot_hist_log(lst1, lst2, leg1, leg2, xlable, ylable, outpath, region):

    nbins=100

    rmin = min(min(lst1),min(lst2))
    rmax = max(max(lst1),max(lst2))

    if xlable=="sieie": rmax = min(max(max(lst1),max(lst2)), 0.08)

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    hist2, bins2 = np.histogram(lst2, bins=nbins, density=True, range=(rmin, rmax))
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    hep.histplot(
        hist2,
        bins=bins2,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="red",
        label=leg2,
        ax=ax,
    )


    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s_log/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all": ax.set_xlabel(xlable_keys[xlable], fontsize=18)
    else: ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)

    
    ax.set_ylabel(ylable, fontsize=18)
    ax.set_yscale('log')
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s_%s_log.png"%(path_to_save, xlable, region))
    fig.savefig("%s%s_%s_log.pdf"%(path_to_save, xlable, region))
    fig.clf()


def plot_2dhist(lst1, lst2, xlable, ylable, outpath, region, eve):

    nbins=100

    rmax_keys = {"r9": 2,
    "sieie": min(max(lst1), 0.08),
    "chargedHadronIso": 80,
    "neutralHadronIso": 30,
    "photonIso": 100,
    "relchargedHadronIso": 1.5,
    "relneutralHadronIso": 0.8,
    "relphotonIso": 2,
    "hadronicOverEm": 0.6,
    "pt": 300,
    "eta": 3,
    "phi": 3
    }


    print("\t corrcoef of %s and %s for %s_%s = "%(xlable, ylable, eve, region), np.corrcoef(np.array(lst1), np.array(lst2))[1,0], "\n")

    hist, xbin, ybin = np.histogram2d(lst1, lst2, bins=nbins, density=True, range=[[min(lst1), rmax_keys[xlable]], [min(lst2),rmax_keys[ylable]]])
    
    
    fig, ax = plt.subplots(figsize=(7, 7))
    hep.hist2dplot(
        hist,
        xbins=xbin,
        ybins=ybin,
        color="b",
        alpha=1,
        cmin=1e-4,
        edgecolor=None,
        cbarextend=True,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s/"%(outpath, region)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    if region=="all":
        ax.set_xlabel(xlable_keys[xlable], fontsize=18)
        ax.set_ylabel(xlable_keys[ylable], fontsize=18)
    else:
        ax.set_xlabel(xlable_keys[xlable]+" (%s)"%region, fontsize=18)
        ax.set_ylabel(xlable_keys[ylable]+" (%s)"%region, fontsize=18)

    ax.set_title(eve, fontsize=18)
    

    fig.show()
    fig.savefig("%s2d%s_%s_vs_%s_%s.png"%(path_to_save, eve, ylable, xlable, region))
    fig.savefig("%s2d%s_%s_vs_%s_%s.pdf"%(path_to_save, eve, ylable, xlable, region))
    fig.clf()




def plot_validation(lst1, leg1, xlable, ylable, outpath, valid_k):

    nbins=100

    #rmax_keys = {"genpt": 1,
    #"pt_by_genpt": r"$p_T$/Gen. $p_T$",
    #"dR": r"$\Delta$ R"
    #}

    rmax = max(lst1)



    rmin = min(lst1)

    hist1, bins1 = np.histogram(lst1, bins=nbins, density=True, range=(rmin, rmax))
    
    
    fig, ax = plt.subplots(figsize=(10, 7))
    hep.histplot(
        hist1,
        bins=bins1,
        #histtype="fill",
        color="b",
        alpha=1,
        edgecolor="black",
        label=leg1,
        ax=ax,
    )

    

    if not os.path.exists(outpath): os.mkdir(outpath)
    path_to_save = "%s/%s_%s/"%(outpath, "valid", leg1)
    if not os.path.exists(path_to_save): os.mkdir(path_to_save)

    ax.set_xlabel(valid_k[xlable], fontsize=18)
    ax.set_ylabel(ylable, fontsize=18)
    #ax.set_xlim(rmin, rmax)
    ax.legend()
    fig.show()
    fig.savefig("%s%s.png"%(path_to_save, xlable))
    fig.savefig("%s%s.pdf"%(path_to_save, xlable))
    fig.clf()


def calculate_dR(electron_candidate):
     
    photon_reco = ROOT.TLorentzVector()
    photon_reco.SetPtEtaPhiE(electron_candidate.pt(), electron_candidate.eta(), electron_candidate.phi(), electron_candidate.energy())

    photon_gen = ROOT.TLorentzVector()
    photon_gen.SetPtEtaPhiE(electron_candidate.genParticle().pt(), electron_candidate.genParticle().eta(), electron_candidate.genParticle().phi(), electron_candidate.genParticle().energy())
     
    return photon_reco.DeltaR(photon_gen)

def photon_fake_candidate(electron_candidate, genParticles):
    """
    This function checks on truth level if the photon candidate stems from a jet.
    Loops through all genJets and genParticles and checks if
    the closest object at generator level is a prompt photon, prompt electron, prompt muon or stems from a jet.
    Returns True / False
    """

    min_DeltaR = float(99999)

    electron_vector = ROOT.TLorentzVector()
    electron_vector.SetPtEtaPhiE(electron_candidate.pt(), electron_candidate.eta(), electron_candidate.phi(), electron_candidate.energy())

    # print("\t\t Photon gen particle PDG ID:", electron_candidate.genParticle().pdgId())

    # this jet loop might be not needed... check later, but doesn't harm at this point
    #jet_around_photon = False
    #for genJet in genJets:
        # build four-vector to calculate DeltaR to photon
    #    genJet_vector = ROOT.TLorentzVector()
    #    genJet_vector.SetPtEtaPhiE(genJet.pt(), genJet.eta(), genJet.phi(), genJet.energy())

    #    DeltaR = electron_vector.DeltaR(genJet_vector)
        # print("\t\t INFO: gen jet eta, phi, delta R ", genJet.eta(), genJet.phi(), DeltaR)
    #    if DeltaR < 0.3:
    #        jet_around_photon = True

    is_prompt = False
    pdgId = 0
    pt_fake = float(99999)

    for genParticle in genParticles:

        if genParticle.pt() < 1: continue # threshold of 1GeV for interesting particles

        # build four-vector to calculate DeltaR to photon
        genParticle_vector = ROOT.TLorentzVector()
        genParticle_vector.SetPtEtaPhiE(genParticle.pt(), genParticle.eta(), genParticle.phi(), genParticle.energy())
        
        DeltaR = electron_vector.DeltaR(genParticle_vector)
        # print("\t\t INFO: gen particle eta, phi, delta R ", genParticle.eta(), genParticle.phi(), DeltaR)
        if DeltaR < min_DeltaR and DeltaR < 0.3:
            min_DeltaR = DeltaR
            pdgId = genParticle.pdgId()
            is_prompt = genParticle.isPromptFinalState()
            pt_fake = genParticle.pt()
            
    #print("\t pdgId=",pdgId)
    #print("\t is_prompt=",is_prompt)
        # print("\t\t INFO: PDG ID:", pdgId)

    #prompt_electron = True if (abs(pdgId)==11 and is_prompt) else False
    prompt_photon = True if (pdgId==22 and is_prompt) else False
    #prompt_muon = True if (abs(pdgId)==13 and is_prompt) else False

    fakept_by_recopt = pt_fake/electron_candidate.pt()
    
    #print("\t prompt_electron=",prompt_electron)
    #print("\t prompt_photon=",prompt_photon)
    #print("\t prompt_muon=",prompt_muon)
    
    if prompt_photon and fakept_by_recopt<1.2 and fakept_by_recopt>0.8:
        return [True, fakept_by_recopt, min_DeltaR]
    else:
        return [False]
    







xlable_keys = {"r9": r"$5 \times 5$ $R_9$",
    "sieie": r"$5 \times 5$ $\sigma_{i\eta i\eta}$",
    "chargedHadronIso": r"Charged hadron isolation [GeV]",
    "neutralHadronIso": r"Neutral hadron isolation [GeV]",
    "photonIso": r"Photon isolation [GeV]",
    "relchargedHadronIso": r"Rel. charged hadron isolation",
    "relneutralHadronIso": r"Rel. neutral hadron isolation",
    "relphotonIso": r"Rel. photon isolation",
    "hadronicOverEm": r"H/E",
    "pt": r"$p_T$ [GeV]",
    "eta": r"$\eta$",
    "phi": r"$\phi$ [rad]"
    }


all_list_true = {}

EB_list_true = {}

EE_list_true = {}


all_list_fake = {}

EB_list_fake = {}

EE_list_fake = {}

for key in xlable_keys.keys():
    all_list_true[key] = []
    EB_list_true[key] = []
    EE_list_true[key] = []

    all_list_fake[key] = []
    EB_list_fake[key] = []
    EE_list_fake[key] = []





valid_keys = {"genpt": r"Gen. $p_T$ [GeV]",
    "pt_by_genpt": r"$p_T$/Gen. $p_T$",
    "dR": r"$\Delta$ R"
}

valid = {}
for key in valid_keys.keys():
    valid[key] = []

valid_keys_b = {
    "fakept_by_recopt": r"Fake photon $p_T$/ Reco. $p_T$",
    "dR": r"$\Delta$ R"
}

valid_b = {}
for key in valid_keys_b.keys():
    valid_b[key] = []




electronHandle, electronLabel = Handle("std::vector<pat::Electron>"), "slimmedElectrons"
genParticlesHandle, genParticlesLabel = Handle("std::vector<reco::GenParticle>"), "prunedGenParticles"
genJetsHandle, genJetsLabel = Handle("std::vector<reco::GenJet>"), "slimmedGenJets"


path_GJet_files = "/eos/cms//store/group/phys_egamma/ec/nkasarag/DYto2E_M-50/Pt-50toInf/DYto2E_M-50_NNPDF31_TuneCP5_13p6TeV-powheg-pythia8/DYto2E_M-50/230904_193739/0000/"

filenames = os.listdir(path_GJet_files)
    # just take one file for tests
filenames = filenames[:20]

file_num = 0

for filename in filenames:


    file_num+=1
    print("\n \t ### processing file number: %s ###"%str(file_num))

    events = Events(path_GJet_files+filename)



    stop_index = -100000
    for i,event in enumerate(events):

            if i == stop_index: 
                break 
        
            if i % 1000 == 0:
                print("\t \t INFO: processing event", i)

            event.getByLabel(electronLabel, electronHandle)
            event.getByLabel(genParticlesLabel, genParticlesHandle)
            event.getByLabel(genJetsLabel, genJetsHandle)

            #genJets = genJetsHandle.product()
            genParticles = genParticlesHandle.product()
        
            #print(i, " genJets: ", genJets)
            #print(i, " genParticles: ", genParticles)


            #print("\n \t event =",i+1)
            
            for electron in electronHandle.product():
                #print("\t et = %s"%electron.et())
                #print("\t pt = %s"%electron.pt())
                #print("\t energy = %s"%electron.energy())
                #print("\t r9 = %s"%electron.full5x5_r9())
                #print("\t sieie = %s"%electron.full5x5_sigmaIetaIeta())
                #print("\t hadronicOverEm = %s"%electron.hadronicOverEm())
                #print("\t chargedHadronIso = %s"%electron.chargedHadronIso())
                #print("\t neutralHadronIso = %s"%electron.neutralHadronIso())
                #print("\t photonIso = %s"%electron.photonIso())
                #print("\t subdetId = %s"%electron.superCluster().seed().seed().subdetId(), "\t photonEta = %s"%photon.eta())

                is_real = False
                is_photon_fake = False
                if electron.pt() < 20: continue
                
                try:
                    pdgId = electron.genParticle().pdgId()
                    if abs(pdgId) == 11:
                        is_real = True
            
                except ReferenceError:
                    is_photon_fake = photon_fake_candidate(electron, genParticles)[0]
                    
                #print("\t jet=",hadronic_fake_candidate(photon, genJets, genParticles))  
                #print("\t is_real=",is_real)
                #print("\t is_photon_fake=", is_photon_fake)


                if is_real: 
                    all_list_true["r9"].append(electron.full5x5_r9())
                    all_list_true["sieie"].append(electron.full5x5_sigmaIetaIeta())
                    all_list_true["chargedHadronIso"].append(electron.chargedHadronIso())
                    all_list_true["neutralHadronIso"].append(electron.neutralHadronIso())
                    all_list_true["photonIso"].append(electron.photonIso())
                    all_list_true["relchargedHadronIso"].append(electron.chargedHadronIso()/electron.pt())
                    all_list_true["relneutralHadronIso"].append(electron.neutralHadronIso()/electron.pt())
                    all_list_true["relphotonIso"].append(electron.photonIso()/electron.pt())
                    all_list_true["hadronicOverEm"].append(electron.hadronicOverEm())
                    all_list_true["pt"].append(electron.pt())
                    all_list_true["eta"].append(electron.eta())
                    all_list_true["phi"].append(electron.phi())

                    valid["genpt"].append(electron.genParticle().pt())
                    valid["pt_by_genpt"].append(electron.pt()/electron.genParticle().pt())
                    valid["dR"].append(calculate_dR(electron))


                    if electron.superCluster().seed().seed().subdetId()==1:
                        EB_list_true["r9"].append(electron.full5x5_r9())
                        EB_list_true["sieie"].append(electron.full5x5_sigmaIetaIeta())
                        EB_list_true["chargedHadronIso"].append(electron.chargedHadronIso())
                        EB_list_true["neutralHadronIso"].append(electron.neutralHadronIso())
                        EB_list_true["photonIso"].append(electron.photonIso())
                        EB_list_true["relchargedHadronIso"].append(electron.chargedHadronIso()/electron.pt())
                        EB_list_true["relneutralHadronIso"].append(electron.neutralHadronIso()/electron.pt())
                        EB_list_true["relphotonIso"].append(electron.photonIso()/electron.pt())
                        EB_list_true["hadronicOverEm"].append(electron.hadronicOverEm())
                        EB_list_true["pt"].append(electron.pt())
                        EB_list_true["eta"].append(electron.eta())
                        EB_list_true["phi"].append(electron.phi())

                    elif electron.superCluster().seed().seed().subdetId()==2:
                        EE_list_true["r9"].append(electron.full5x5_r9())
                        EE_list_true["sieie"].append(electron.full5x5_sigmaIetaIeta())
                        EE_list_true["chargedHadronIso"].append(electron.chargedHadronIso())
                        EE_list_true["neutralHadronIso"].append(electron.neutralHadronIso())
                        EE_list_true["photonIso"].append(electron.photonIso())
                        EE_list_true["relchargedHadronIso"].append(electron.chargedHadronIso()/electron.pt())
                        EE_list_true["relneutralHadronIso"].append(electron.neutralHadronIso()/electron.pt())
                        EE_list_true["relphotonIso"].append(electron.photonIso()/electron.pt())
                        EE_list_true["hadronicOverEm"].append(electron.hadronicOverEm())
                        EE_list_true["pt"].append(electron.pt())
                        EE_list_true["eta"].append(electron.eta())
                        EE_list_true["phi"].append(electron.phi())

                    else: continue

                if is_photon_fake: 
                    all_list_fake["r9"].append(electron.full5x5_r9())
                    all_list_fake["sieie"].append(electron.full5x5_sigmaIetaIeta())
                    all_list_fake["chargedHadronIso"].append(electron.chargedHadronIso())
                    all_list_fake["neutralHadronIso"].append(electron.neutralHadronIso())
                    all_list_fake["photonIso"].append(electron.photonIso())
                    all_list_fake["relchargedHadronIso"].append(electron.chargedHadronIso()/electron.pt())
                    all_list_fake["relneutralHadronIso"].append(electron.neutralHadronIso()/electron.pt())
                    all_list_fake["relphotonIso"].append(electron.photonIso()/electron.pt())
                    all_list_fake["hadronicOverEm"].append(electron.hadronicOverEm())
                    all_list_fake["pt"].append(electron.pt())
                    all_list_fake["eta"].append(electron.eta())
                    all_list_fake["phi"].append(electron.phi())

                    valid_b["fakept_by_recopt"].append(photon_fake_candidate(electron, genParticles)[1])
                    valid_b["dR"].append(photon_fake_candidate(electron, genParticles)[2])


                    if electron.superCluster().seed().seed().subdetId()==1:
                        EB_list_fake["r9"].append(electron.full5x5_r9())
                        EB_list_fake["sieie"].append(electron.full5x5_sigmaIetaIeta())
                        EB_list_fake["chargedHadronIso"].append(electron.chargedHadronIso())
                        EB_list_fake["neutralHadronIso"].append(electron.neutralHadronIso())
                        EB_list_fake["photonIso"].append(electron.photonIso())
                        EB_list_fake["relchargedHadronIso"].append(electron.chargedHadronIso()/electron.pt())
                        EB_list_fake["relneutralHadronIso"].append(electron.neutralHadronIso()/electron.pt())
                        EB_list_fake["relphotonIso"].append(electron.photonIso()/electron.pt())
                        EB_list_fake["hadronicOverEm"].append(electron.hadronicOverEm())
                        EB_list_fake["pt"].append(electron.pt())
                        EB_list_fake["eta"].append(electron.eta())
                        EB_list_fake["phi"].append(electron.phi())

                    elif electron.superCluster().seed().seed().subdetId()==2:
                        EE_list_fake["r9"].append(electron.full5x5_r9())
                        EE_list_fake["sieie"].append(electron.full5x5_sigmaIetaIeta())
                        EE_list_fake["chargedHadronIso"].append(electron.chargedHadronIso())
                        EE_list_fake["neutralHadronIso"].append(electron.neutralHadronIso())
                        EE_list_fake["photonIso"].append(electron.photonIso())
                        EE_list_fake["relchargedHadronIso"].append(electron.chargedHadronIso()/electron.pt())
                        EE_list_fake["relneutralHadronIso"].append(electron.neutralHadronIso()/electron.pt())
                        EE_list_fake["relphotonIso"].append(electron.photonIso()/electron.pt())
                        EE_list_fake["hadronicOverEm"].append(electron.hadronicOverEm())
                        EE_list_fake["pt"].append(electron.pt())
                        EE_list_fake["eta"].append(electron.eta())
                        EE_list_fake["phi"].append(electron.phi())

                    else: continue


out_path = "/eos/user/n/nkasarag/www/folder/non_isolated_photonID/DYto2E_20files_with_rel_pt_cut"
#out_path = "/eos/user/n/nkasarag/www/folder/non_isolated_photonID/test_electron"


for key in xlable_keys.keys():
    plot_hist(all_list_true[key], all_list_fake[key], "signal", "background", key, "Normalized entries", out_path, "all")
    plot_hist(EB_list_true[key], EB_list_fake[key], "signal", "background", key, "Normalized entries", out_path, "EB")
    plot_hist(EE_list_true[key], EE_list_fake[key], "signal", "background", key, "Normalized entries", out_path, "EE")

    plot_hist_log(all_list_true[key], all_list_fake[key], "signal", "background", key, "Normalized entries", out_path, "all")
    plot_hist_log(EB_list_true[key], EB_list_fake[key], "signal", "background", key, "Normalized entries", out_path, "EB")
    plot_hist_log(EE_list_true[key], EE_list_fake[key], "signal", "background", key, "Normalized entries", out_path, "EE")


for key in valid_keys.keys():
    plot_validation(valid[key], "signal", key, "Normalized entries", out_path, valid_keys)

for key in valid_keys_b.keys():
    plot_validation(valid_b[key], "background", key, "Normalized entries", out_path, valid_keys_b)




hist_key_2d = {"1": ["r9", "sieie"],
               "2": ["r9", "hadronicOverEm"],
               "3": ["sieie", "hadronicOverEm"]
}

for key in hist_key_2d.keys():
    plot_2dhist(all_list_true[hist_key_2d[key][0]], all_list_true[hist_key_2d[key][1]], hist_key_2d[key][0], hist_key_2d[key][1], out_path, "all", "signal")
    plot_2dhist(all_list_fake[hist_key_2d[key][0]], all_list_fake[hist_key_2d[key][1]], hist_key_2d[key][0], hist_key_2d[key][1], out_path, "all", "background")

    plot_2dhist(EB_list_true[hist_key_2d[key][0]], EB_list_true[hist_key_2d[key][1]], hist_key_2d[key][0], hist_key_2d[key][1], out_path, "EB", "signal")
    plot_2dhist(EB_list_fake[hist_key_2d[key][0]], EB_list_fake[hist_key_2d[key][1]], hist_key_2d[key][0], hist_key_2d[key][1], out_path, "EB", "background")

    plot_2dhist(EE_list_true[hist_key_2d[key][0]], EE_list_true[hist_key_2d[key][1]], hist_key_2d[key][0], hist_key_2d[key][1], out_path, "EE", "signal")
    plot_2dhist(EE_list_fake[hist_key_2d[key][0]], EE_list_fake[hist_key_2d[key][1]], hist_key_2d[key][0], hist_key_2d[key][1], out_path, "EE", "background")